##############################################
######### IMPORTS & DECLARATION ##############
##############################################

import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()

##############################################
############### EXTRACTION ###################
##############################################

redshift_details_source = {
    "url": "jdbc:redshift://vy-edw-insight-cluster-prod.ciiqwesejii6.us-east-2.redshift.amazonaws.com:5439/pve",
    "user": "pmuralidharan",
    "password": "RSVya1reP4d@",
    "dbtable": 'public.glb_fiar_ln_emagia',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

try:
    source_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift",connection_options=redshift_details_source,transformation_ctx="source_dynamic_frame")
    logger.info("source_dynamic_frame formed")
except Exception as e:
    logger.info(f"Excption during {e.args}")
    

##############################################
################## LOAD ######################
##############################################

redshift_details_target = {
    "url": "jdbc:redshift://vy-edw-insight-cluster-dev1.cm5u33ejahqp.us-east-2.redshift.amazonaws.com:5439/dve",
    "user": "etl_di_user01",
    "password": "etlVyD4v",
    "dbtable": 'glb_emagia.glb_fiar_ln_emagia',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

try:
    glueContext.write_dynamic_frame.from_options(frame=source_dynamic_frame,connection_type="redshift",connection_options=redshift_details_target)
    logger.info("data written to redshift")
except Exception as e:
    logger.info(f"Excption during {e.args}")

logger.info("job completed")

job.commit()