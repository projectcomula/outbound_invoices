##############################################
######### IMPORTS & DECLARATION ##############
##############################################

import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame
import boto3

# @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()


##############################################
############### EXTRACTION ###################
##############################################

redshift_details = {
    "url": "jdbc:redshift://vy-edw-insight-cluster-dev1.cm5u33ejahqp.us-east-2.redshift.amazonaws.com:5439/dve",
    "user": "etl_di_user01",
    "password": "etlVyD4v",
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

try:
    redshift_details["dbtable"] = "glb_emagia.glb_fiar_ln_emagia"
    emagia_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details, transformation_ctx="emagia_dynamic_frame")

    redshift_details["dbtable"] = "glb_revenue.glb_revenue_ln"
    revenue_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details, transformation_ctx="revenue_dynamic_frame")

    redshift_details["dbtable"] = "glb_cust_mstr.glb_cust"
    cust_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details, transformation_ctx="cust_dynamic_frame")

    redshift_details["dbtable"] = "glb_cust_mstr.glb_cust_gd"
    cust_gd_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details, transformation_ctx="cust_gd_dynamic_frame")

    redshift_details["dbtable"] = "jde_tables.jde_f4101_material"
    jde_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details, transformation_ctx="jde_dynamic_frame")

    redshift_details["dbtable"] = "glb_ordmgmt.glb_ord_mgmt_ln"
    order_dynamic_frame = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details, transformation_ctx="order_dynamic_frame")

    logger.info("Dynamic frames created for all 6 tables")

except Exception as e:

    logger.exception(f"Exception during Dynamic frame formation{e.args}")

##############################################
############## TRANSFORMATION ################
##############################################

try:

    emagia_dataframe = emagia_dynamic_frame.toDF().repartition(1)
    emagia_dataframe.createOrReplaceTempView("emagia_final_table")

    revenue_dataframe = revenue_dynamic_frame.toDF().repartition(1)
    revenue_dataframe.createOrReplaceTempView("revenue_table")

    cust_dataframe = cust_dynamic_frame.toDF().repartition(1)
    cust_dataframe.createOrReplaceTempView("cust_table")

    cust_gd_dataframe = cust_gd_dynamic_frame.toDF().repartition(1)
    cust_gd_dataframe.createOrReplaceTempView("cust_gd_table")

    jde_dataframe = jde_dynamic_frame.toDF().repartition(1)
    jde_dataframe.createOrReplaceTempView("jde_table")

    order_dataframe = order_dynamic_frame.toDF().repartition(1)
    order_dataframe.createOrReplaceTempView("order_table")

    logger.info("Dataframes created for all 6 tables")

except Exception as e:

    logger.exception(f"Exception during Dataframe formation{e.args}")

try:
    
    s3 = boto3.client('s3')
    path = "aws-glue/outbound/source-sql/jde"
    bucket = 'glb-emagia-dev'
    result = s3.list_objects(Bucket=bucket, Prefix=path)

    for item in result.get('Contents'):

        data = s3.get_object(Bucket=bucket, Key=item.get('Key'))
        contents = data['Body'].read()
        jde_query = contents.decode("utf-8")

    logger.info(f"Before replace : {jde_query}")

    jde_query = jde_query.replace('public.glb_fiar_ln_emagia', 'emagia_final_table')
    jde_query = jde_query.replace('glb_revenue.glb_revenue_ln', 'revenue_table')
    jde_query = jde_query.replace('glb_cust_mstr.glb_cust_gd', 'cust_gd_table')
    jde_query = jde_query.replace('glb_cust_mstr.glb_cust', 'cust_table')
    jde_query = jde_query.replace('GLB_CUST_MSTR.glb_cust', 'cust_table')
    jde_query = jde_query.replace('jde_tables.jde_f4101_material', 'jde_table')
    jde_query = jde_query.replace('glb_ordmgmt.glb_ord_mgmt_ln', 'order_table')
    jde_query = jde_query.replace('"type"', 'atype')

    logger.info(f"After replace : {jde_query}")

except Exception as e:

    logger.exception(f"Exception during s3 retreival {e.args}")

try:

    logger.info("Querying the in memory tables for header table data")
    header_df = spark.sql(jde_query)
    logger.info(f"Header DF count : {header_df.count()}")

except Exception as e:

    logger.exception(f"Exception during SQL {e.args}")

##############################################
################## LOAD ######################
##############################################

# Write to csv

try:

    header_df.coalesce(1).write.option("header","true").csv("s3://glb-emagia-dev/aws-glue/outbound/output-files/header-table/jde")

except Exception as e:

    logger.exception(f"Exception during s3 write {e.args}")

# Write to Redshift

header_dynamic_frame = DynamicFrame.fromDF(header_df, glueContext, "header_dynamic_frame")

try:

    redshift_details["dbtable"] = "glb_emagia.glb_ar_col_emagia_hdr"
    glueContext.write_dynamic_frame.from_options(frame=header_dynamic_frame, connection_type="redshift", connection_options=redshift_details)

    logger.info("Data written to redshift")

except Exception as e:

    logger.exception(f"Exception during redshift write {e.args}")

logger.info("job completed")

job.commit()
