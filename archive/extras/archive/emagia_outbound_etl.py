
##############################################
######### IMPORTS & DECLARATION###############
##############################################
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from pyspark.sql import SQLContext
from awsglue.context import GlueContext
import awswrangler as wr
from awsglue.job import Job
import boto3
from awsglue.dynamicframe import DynamicFrame
import base64
import json
import pandas as pd


args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
sqlContext = SQLContext(sc)
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()

#VARIABLE DECLARATION

SECRET = 'rdbcluster_credentials'
REGION = 'us-east-2'


#SECRET KEY EXTRACTION
def getSecret(secretName, regionName):

    try:
        client = boto3.client(service_name = 'secretsmanager', region_name = regionName)
        secretFromAWS = client.get_secret_value(SecretId = secretName)

        if 'SecretString' in secretFromAWS:
            secret = secretFromAWS['SecretString']
            logger.info("Secret retrieved successfully")
            return json.loads(secret)
        else:
            decoded_binary_secret = base64.b64decode(secretFromAWS['SecretBinary'])
            return decoded_binary_secret  

    except Exception as e:
        logger.info(f"Error in contacting AWS :: {e.args}")
        return None
        
rs_secret = getSecret(SECRET, REGION)
rs_url = f"jdbc:redshift://{rs_secret['host']}:{rs_secret['port']}/{rs_secret['database']}"
logger.info(f'Redshift URL : {rs_url}')

# JDBC CONNECTION STRINGS
redshift_details_source = {
    "url": rs_url,
    "user": rs_secret['username'],
    "password": rs_secret['password'],
    "dbtable": 'public.gglb_fiar_ln_emagia',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}


redshift_details_source1 = {
    "url": rs_url,
    "user": rs_secret['username'],
    "password": rs_secret['password'],
    "dbtable": 'public.glb_revenue_ln',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

redshift_details_source2 = {
    "url": rs_url,
    "user": rs_secret['username'],
    "password": rs_secret['password'],
    "dbtable": 'public.glb_cust',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

redshift_details_target = {
    "url": rs_url,
    "user": rs_secret['username'],
    "password": rs_secret['password'],
    "dbtable": 'public.apollo_inactivity_salesorglinenew',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}
rev_conn = {
    "url": rs_url,
    "user": rs_secret['username'],
    "password": rs_secret['password'],
    "dbtable": 'public.apollo_inactivity_salesorg_temp123',
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

############################
#######DATA EXTRACTION######
############################

stage_df = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details_source,transformation_ctx="stage_df")
#revenue_dynamicframe = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details_source1, transformation_ctx="revenue_dynamicframe")
#customer_dynamicframe = glueContext.create_dynamic_frame_from_options(connection_type="redshift", connection_options=redshift_details_source2, transformation_ctx="revenue_dynamicframe")


# DynamicFrame to Spark Frame

source_dataframe = stage_df.toDF().repartition(1)
#revenue_dataframe = revenue_dynamicframe.toDF().repartition(1)
#customer_dataframe = customer_dynamicframe.toDF().repartition(1)


############################
#DATA TRANSFORMATION########
############################

header_list = ['source_id','billing_doc','billing_doc','comp_code','bill_to','bill_date','net_due_date','invc_pmt_terms','posting_date','m_rate_curr_lc','bill_type','doc_curr','deb_cre_amt_lc','deb_cre_amt_dc','deb_cre_amt_dc','tax_amount','unpaid_amt','unpaid_amt_lc','unpaid_amt','unpaid_amt_lc','deb_cre_amt_gc','unpaid_amt_gc','net_due_date','m_rate_curr_gc']

revenue_list = ['sold_to','fr_net_revenue_dc']
customer_list = ['customer_region']

# SLICING

header_dataframe = source_dataframe[[header_list]]
#df2=revenue_dataframe[[revenue_list]]
#df3=customer_dataframe[[customer_list]]

################################
        # DATA LOADING
################################
try:
    source_dataframe.write.option("header",true).csv('s3://consolidatedar/temp/csv')   

except Exception as E:
    logger.info(f"Error in Writing:: {E.args}")


logger.info('Job CompleTed')

job.commit()