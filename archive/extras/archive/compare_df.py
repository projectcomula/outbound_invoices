import pandas as pd
import numpy as np
import boto3
import io
s3 = boto3.client('s3')

obj = s3.get_object(Bucket='consolidatedar', Key='compare.xlsx')
df = pd.read_excel(io.BytesIO(obj['Body'].read()))
#print(df)
df2=df.copy()
df2.loc[0, 'Address'] = 'Chennai'
df2.loc[2, 'Blood_group'] = 'O'
df2.loc[9, 'Address'] = 'Chennai'
#print(df2)
df3 = df.compare(df2,align_axis=1)
s=(df3.index.values)
print(s)
print(df2.iloc[s])

