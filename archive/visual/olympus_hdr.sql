select 
ft.account_doc as unique_id
,ft.account_doc as number
,null as trx_number
,null as id_year
,ft.comp_code as company_code
,case 
when account_doc_type='DG' then 'CM'
when account_doc_type ='AB' and  deb_cre_amt_dc<0  then 'CM'
when account_doc_type='AB' and deb_cre_amt_dc>=0  then 'INV'
when account_doc_type='RV' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='RV' and  deb_cre_amt_dc>=0  then 'INV' 
when account_doc_type='DZ' then 'CM'
when account_doc_type='DR'  then 'INV'
when account_doc_type='SA' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='SA' and  deb_cre_amt_dc>=0  then 'DM' 
else ''
end as type
,null as order_type
,ft.bill_date as trx_date
,net_due_date as trx_due_date
,sum(deb_cre_amt_dc) as amount
,sum(deb_cre_amt_lc) as inv_amount
,rev.sold_to as sold_to_customer_id
,ft.bill_to as bill_to_customer_id
,invc_pmt_terms as term
,null as po_number
,null as po_date
,null as so_number
,null as so_date
,null as internal_notes
,m_rate_curr_lc as exchange_rate
,doc_curr as invoice_curr_code
,case 
when unpaid_amt_gc =0 then 'Y'
when unpaid_amt_gc =deb_cre_amt_gc then 'N'
when unpaid_amt_gc<deb_cre_amt_gc and unpaid_amt_gc>0 then 'P'
else null
end as complete_flag
,sum(unpaid_amt) as over_due_amount
,sum(unpaid_amt_lc) as func_over_due_amount
,case 
when net_due_date>CURRENT_DATE then  sum(unpaid_amt)
else 0
end as  curr_due_amount
,null as last_paid_amount
,case 
when net_due_date<CURRENT_DATE then  sum(unpaid_amt_lc)
else 0
end as func_curr_due_amount
,null as last_paid_date
,null as check_number
,null as delivery_note_number
,null as rma_numbers 
,null as related_cust_trx_id 
,null as related_trx_year 
,null as related_cust_trx_date
,null as ship_to_contact_id 
,null as bill_to_contact_id 
,null as lob_code 
,null as sold_to_site_use_id 
,null as bill_to_site_use_id 
,null as primary_salesrep_id 
,null as outside_salesrep_id 
,sum(ft.tax_amount ) as tax_amount
,null as freight_amount
,null as other_charges1
,null as other_charges2
,'N' as reversed
,null as lob_name
,cust_region.customer_region as region
,null as project_number
,null as contract_number
,null as contract_date
,null as ship_via
,null as ship_ware_house
,null as reversal_gl_date
,sum(deb_cre_amt_dc) as revenue_amount
,null as func_revenue_amt
,null as original_trx_type
,posting_date as gl_date
,sum(deb_cre_amt_gc) as cons_amount
,sum(unpaid_amt_gc) as cons_over_due_amount
,case 
when net_due_date>CURRENT_DATE then  sum(unpaid_amt_gc)
else 0
end as cons_curr_due_amount
,null as cons_last_paid_amt
,null as cons_revenue_amt
,m_rate_curr_gc as cons_exchange_rate
,null as customer_service_rep_id
,null as tracking_no
,null as invoice_header_text
,null as courier_company_code
,case 
when ft.source_id='OLYMPUS' and  ft.gl_account='0000012300' then 'YES'
else 'NO'
end as Intercompany_flag
,ft.source_id
,null as remark
,ft.payer as payer_customer_id
,null as invoice_status_code
from (
select  source_id ,account_doc,comp_code,doc_curr,net_due_date,posting_date,bill_to, sum(deb_cre_amt_dc)as deb_cre_amt_dc,sum(deb_cre_amt_lc) as deb_cre_amt_lc,account_doc_type,invc_pmt_terms,
bill_date,sum(unpaid_amt) as unpaid_amt,invc_status_cd,sum(deb_cre_amt_gc)as deb_cre_amt_gc,m_rate_curr_lc,m_rate_curr_gc,sum(unpaid_amt_lc) as unpaid_amt_lc,
sum(unpaid_amt_gc) as unpaid_amt_gc ,sum(tax_amount) as tax_amount,daily_monthly_flag,gl_account,payer
from emagia_table where source_id ='OLYMPUS'and daily_monthly_flag ='D' and account_doc_type in ('DG','AB','RV','DZ','DR','SA')
and gl_account in ('0000012100','0000012300') group by  source_id ,account_doc,comp_code,doc_curr,net_due_date,
posting_date,bill_to,account_doc_type,invc_pmt_terms,bill_date,invc_status_cd,m_rate_curr_lc,m_rate_curr_gc,daily_monthly_flag,gl_account,payer
)  ft
--where ft.source_id ='JDE' and daily_monthly_flag='D'
left join 
(
select distinct
source_id ,
customer_number,
customer_region, 
customer_reporting_country 
from cust_table where source_id ='OLYMPUS'
)cust_region
on ft.bill_to =cust_region.customer_number 
and ft.source_id =cust_region.source_id 
left join
(
select 
billing_doc,
source_id,
--bill_date,
sold_to,
comp_code,
bill_to
from   revenue_table where source_id='OLYMPUS'
group by 1,2,3,4,5
)rev 
on
ft.account_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.bill_date=rev.bill_date
and ft.bill_to=rev.bill_to
and  ft.comp_code=rev.comp_code
where
ft.source_id = 'OLYMPUS'
and daily_monthly_flag = 'D'
and account_doc_type in ('DG','AB','RV','DZ','DR','SA')
and ft.gl_account in ('0000012100','0000012300')
group by
ft.source_id
,ft.account_doc
,number
,company_code
,sold_to_customer_id
,bill_to_customer_id
,trx_date
,trx_due_date
,term
,region
,Intercompany_flag
,complete_flag
,gl_date
,reversed
,exchange_rate
,type
,invoice_curr_code
,cons_exchange_rate
,ft.payer