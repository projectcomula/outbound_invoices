def MyTransform (glueContext, dfc) -> DynamicFrameCollection:
    
    from pyspark.sql.types import NullType, StringType, TimestampType
    import pyspark.sql.functions as func
    from pyspark.sql.functions import lit, when, col, udf
    from datetime import datetime, date
    
    newdf = dfc.select(list(dfc.keys())[0]).toDF()
    print("Converted to single file")
    
    today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    newdf = newdf.withColumn('datekey', lit(today))
    newdf = newdf.withColumn("datekey", newdf["datekey"].cast(TimestampType()))
    
    newdf = newdf.select([func.lit(None).cast('string').alias(i.name) if isinstance(i.dataType, NullType) else i.name for i in newdf.schema])
    
    # Change Decimal cols to string
    amt_cols=[
        "other_charges1",
        "cons_last_paid_amt",
        "func_revenue_amt",
        "cons_revenue_amt",
        "last_paid_amount",
        "other_charges2",
        "freight_amount"
    ]
    
    for col in amt_cols:
        newdf = newdf.withColumn(col, newdf[col].cast(StringType()))
    
    print(newdf.printSchema())
    newdyc = DynamicFrame.fromDF(newdf, glueContext, "newdyc")
    print("Dyc frame formed")
    
    return (DynamicFrameCollection({"CustomTransform0": newdyc}, glueContext))