
##############################################
######### IMPORTS & DECLARATION ##############
##############################################

import json
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from pyspark.sql import SQLContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame
import boto3
from pyspark.sql.types import NullType
from pyspark.sql.types import StringType, TimestampType
from datetime import date
from pyspark.sql.functions import lit
import pyspark.sql.functions as F
from datetime import datetime, date
today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")




# @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()

glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()


##############################################
############### EXTRACTION ###################
##############################################

redshift_details = {
    "url": "jdbc:redshift://vy-edw-insight-cluster-dev1.cm5u33ejahqp.us-east-2.redshift.amazonaws.com:5439/dve",
    "user": "etl_di_user01",
    "password": "etlVyD4v",
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

rs_tables = [
    "glb_emagia.glb_fiar_ln_emagia",
    "glb_revenue.glb_revenue_vw_tbl",
    "glb_cust_mstr.glb_cust_gd",
    "glb_cust_mstr.glb_cust",
    "jde_tables.jde_f4101_material"
    ]

try:

    rs_dyn_frames = []

    for table in rs_tables:
        redshift_details["dbtable"] = table
        dynamic_frame = glueContext.create_dynamic_frame_from_options(
            connection_type="redshift",
            connection_options=redshift_details,
            transformation_ctx="dynamic_frame"
            )

        rs_dyn_frames.append(dynamic_frame)
            

    logger.info("Dynamic frames created for all source tables")

except Exception as e:

    logger.exception(f"Exception during Dynamic frame formation :: {e.args}")

##############################################
############## TRANSFORMATION ################
##############################################

try:

    i = 0
    pyspark_tables = []

    for item in rs_dyn_frames:
        i += 1
        temp_tbl = f"temp_tbl_{i}"
        dataframe = item.toDF().repartition(1)
        dataframe.createOrReplaceTempView(temp_tbl)
        pyspark_tables.append(temp_tbl)

    logger.info("PySpark temp tables created for all source tables")

except Exception as e:

    logger.exception(f"Exception during temp table formation :: {e.args}")

try:
    
    s3 = boto3.client('s3')
    path = "aws-glue/outbound/source-sql/apollo_ln"
    bucket = 'glb-emagia-dev'
    result = s3.list_objects(Bucket=bucket, Prefix=path)

    for item in result.get('Contents'):

        data = s3.get_object(Bucket=bucket, Key=item.get('Key'))
        contents = data['Body'].read()
        apollo_lquery = contents.decode("utf-8")

    logger.info(f"Before replace : {apollo_lquery}")

    for a, b in zip(rs_tables, pyspark_tables):
        apollo_lquery = apollo_lquery.replace(a, b)

    apollo_lquery = apollo_lquery.replace('"type"', 'atype')
    logger.info(f"After replace : {apollo_lquery}")

except Exception as e:

    logger.exception(f"Exception during s3 retreival {e.args}")

try:
   


    logger.info("Querying the temp tables for header table data")
    header_df = spark.sql(apollo_lquery)
    logger.info(f"Header DF count : {header_df.count()}")
    #logger.info(header_df.show(1000))

except Exception as e:

    logger.exception(f"Exception during SQL {e.args}")

##############################################
################## LOAD ######################
##############################################


df2 = header_df.select([
    F.lit(None).cast('string').alias(i.name)
    if isinstance(i.dataType, NullType)
    else i.name
    for i in header_df.schema
])



df2 = df2.withColumn("line_qty",df2["line_qty"].cast(StringType()))
df2 = df2.withColumn("unit_price",df2["unit_price"].cast(StringType()))
df2 = df2.withColumn("extended_price",df2["extended_price"].cast(StringType()))
df2 = df2.withColumn("line_amount",df2["line_amount"].cast(StringType()))
df2 = df2.withColumn("tax_amount",df2["tax_amount"].cast(StringType()))
df2 = df2.withColumn("freight_amount",df2["freight_amount"].cast(StringType()))
df2 = df2.withColumn('datekey',lit(today))
df2 = df2.withColumn("datekey",df2["datekey"].cast(TimestampType()))
df2 = df2.withColumnRenamed("company_code","company_id")
df2 = df2.withColumnRenamed("lob_name","loc_name")
df2 = df2.withColumnRenamed("other_1","other_charges1")
df2 = df2.withColumnRenamed("other_2","other_charges2")
df2 = df2.withColumnRenamed("atype","type")

print(df2.printSchema())


'''
# Write to csv
try:
    S3_PATH = f"s3://glb-emagia-dev/aws-glue/outbound/output-files/Apollo/line-item-table/{date.today().strftime('%d%m%Y')}"
    df2.coalesce(1).write.option("header","true").csv(S3_PATH)
     
except Exception as e:
    logger.exception(f"Exception during s3 write {e.args}")

#logger.exception(f"Exception during s3 write {e.args}")
# Write to Redshift
'''

header_dynamic_frame = DynamicFrame.fromDF(df2, glueContext, "header_dynamic_frame")

try: 
     redshift_details["dbtable"] = "glb_emagia.glb_ar_col_emagia_ln_itm"
     glueContext.write_dynamic_frame.from_options(frame=header_dynamic_frame, connection_type="redshift", connection_options=redshift_details)
     logger.info("Data written to redshift")

except Exception as e:

     logger.exception(f"Exception during redshift write {e.args}")

logger.info("job completed")

job.commit()
