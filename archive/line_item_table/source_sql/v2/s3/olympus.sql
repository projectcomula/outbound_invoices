select 
ft.account_doc as id
,rev.billing_item as line_no
,ft.bill_to as customer_id
,null as id_year
,ft.comp_code as company_id
,case 
when account_doc_type='DG' then 'CM'
when account_doc_type ='AB' and  deb_cre_amt_dc<0  then 'CM'
when account_doc_type='AB' and deb_cre_amt_dc>=0  then 'INV'
when account_doc_type='RV' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='RV' and  deb_cre_amt_dc>=0  then 'INV' 
when account_doc_type='DZ' then 'CM'
when account_doc_type='DR'  then 'INV'
when account_doc_type='SA' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='SA' and  deb_cre_amt_dc>=0  then 'DM' 
else ''
end as type
,rev.sales_order as order_number
,rev.line_id as line_number
,"0INCOTERMS" as inco_terms
,null as delivery_number
,rev.material as item_code
,rev.mat_mat_text as item_description
,rev.buom as item_unit
,null as packing_slip_no
,rev.inv_qty_bu as line_qty
,rev.unit_price as unit_price
,rev.extended_price as extended_price
,null as schedule_date
,null as delivery_description
,null as freight_terms
,null as tracking_no
,null as bol_number
,null as invoice_line_text
,null as invoice_header_text
,null as project 
,null as customer_part_no
,null as fiscal_period
,null as ship_to_address
,null as bill_to_address
,null as revision
,null as financial_customer_group
,null as se_number
,null as warehouse
,null as currency_description
,rev.profit_ctr_desc as loc_name
,(rev.unit_price*rev.inv_qty_bu) as line_amount
,(fr.tax_amount ) as tax_amount
,null as freight_amount
,null as other_charges1
,null as other_charges2
,null as total_amount
,null as shipped_date
,null as terms_pay
,null as remitto_id
,null as quote_no
,null as prior_cust
,null as remarks
,rev.po_number as po_number
,rev.po_date
,case 
when ft.source_id='OLYMPUS' and  ft.gl_account='0000012300' then 'YES'
else 'NO'
end as Intercompany_flag
,rev.profit_ctr as lob_code
,ft.source_id
,current_date
from (
select source_id ,account_doc  ,tax_amount,bill_to ,comp_code ,sum(deb_cre_amt_dc) as deb_cre_amt_dc, daily_monthly_flag,account_doc_type,gl_account from emagia_final_table  
where source_id ='OLYMPUS' and daily_monthly_flag='D' and account_doc_type in ('DG','AB','RV','DZ','DR','SA')
and gl_account in ('0000012100','0000012300')
group by source_id ,account_doc  ,tax_amount,bill_to ,comp_code ,daily_monthly_flag,account_doc_type,gl_account
)  ft
--where ft.source_id ='JDE' and daily_monthly_flag='D'
left join
(
select 
billing_doc,
source_id,
case 
when inv_qty_bu is null then 0
when inv_qty_bu=0 then 0
else (sum(out_thedoor_rev_dc)/inv_qty_bu) 
end as  unit_price,
billing_item
,bill_date
,line_id
,sold_to
,comp_code
,material
,mat_mat_text
,inv_qty_bu
,buom
,profit_ctr
,profit_ctr_desc
,po_date
,sales_order
,po_number
,sum(net_revenue_dc) as extended_price
,sum(net_revenue_dc) as line_amount
from   glb_revenue_vw where source_id='OLYMPUS' 
group by 1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17
)rev 
on
ft.account_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.billing_item=rev.billing_item
and ft.comp_code=rev.comp_code
--and ft.bill_to=rev.bill_to
left join 
(
select
	source,
	billing_doc,
	comp_code,
	sum(0) as tax_amount,
	0INCOTERMS
from
	sap_revenue 
group by
	billing_doc
,
	source
,
	comp_code 
,
	0INCOTERMS
)fr
on
ft.account_doc = fr.billing_doc
and ft.source_id = fr.source
and ft.comp_code = fr.comp_code
--and ft.account_doc_type = fr.billing_type
left join 
(
select distinct  0ac_doc_no,0ac_doc_typ ,0comp_code from sap_fiar_line_item 
) ta 
on 
ft.account_doc =ta.0ac_doc_no
and ft.account_doc_type=ta.0ac_doc_typ
and ft.comp_code=ta."0comp_code"
where ft.source_id ='OLYMPUS' and daily_monthly_flag='D'and account_doc_type in ('DG','AB','RV','DZ','DR','SA')
and ft.gl_account in ('0000012100','0000012300')