SELECT  ft.source_id
       ,ft.account_doc                                                                            AS id
       ,ft.bill_to                                                                                AS customer_id
       ,ft.comp_code                                                                              AS company_code
       ,rev.sales_order                                                                           AS order_number
       ,rev.po_number                                                                             AS po_number
       ,rev.po_date
       ,CASE WHEN account_doc_type='DG' THEN 'CM'
             WHEN account_doc_type ='AB' AND deb_cre_amt_dc<0 THEN 'CM'
             WHEN account_doc_type='AB' AND deb_cre_amt_dc>=0 THEN 'INV'
             WHEN account_doc_type='RV' AND deb_cre_amt_dc<0 THEN 'CM'
             WHEN account_doc_type='RV' AND deb_cre_amt_dc>=0 THEN 'INV'
             WHEN account_doc_type='DZ' THEN 'CM'
             WHEN account_doc_type='DR' THEN 'INV'
             WHEN account_doc_type='SA' AND deb_cre_amt_dc<0 THEN 'CM'
             WHEN account_doc_type='SA' AND deb_cre_amt_dc>=0 THEN 'DM'  ELSE '' END              AS "type"
       ,CASE WHEN ft.source_id='OLYMPUS' AND ft.gl_account='0000012300' THEN 'YES'  ELSE 'NO' END AS Intercompany_flag
       ,rev.profit_ctr                                                                            AS lob_code
       ,rev.profit_ctr_desc                                                                       AS lob_name
       ,rev.billing_item                                                                          AS line_no
       ,rev.line_id                                                                               AS line_number
       ,rev.material                                                                              AS item_code
       ,rev.mat_mat_text                                                                          AS item_description
       --,null                                                                                      AS taxable
       ,"0INCOTERMS"                                                                              AS inco_terms
       ,rev.buom                                                                                  AS item_unit
       ,rev.inv_qty_bu                                                                            AS line_qty
       ,rev.unit_price                                                                            AS unit_price
       ,rev.extended_price                                                                        AS extended_price
       ,(rev.unit_price*rev.inv_qty_bu)                                                           AS line_amount
       ,(fr.tax_amount)                                                                           AS tax_amount
       ,null                                                                                      AS freight_amount
       ,null                                                                                      AS other_1
       ,null                                                                                      AS other_2
FROM
(
	SELECT  source_id
	       ,account_doc
	       ,tax_amount
	       ,bill_to
	       ,comp_code
	       ,SUM(deb_cre_amt_dc) AS deb_cre_amt_dc
	       ,daily_monthly_flag
	       ,account_doc_type
	       ,gl_account
	FROM glb_emagia.glb_fiar_ln_emagia
	WHERE source_id ='OLYMPUS'
	AND daily_monthly_flag='D'
	AND account_doc_type IN ('DG', 'AB', 'RV', 'DZ', 'DR', 'SA')
	AND gl_account IN ('0000012100', '0000012300')
	GROUP BY  source_id
	         ,account_doc
	         ,tax_amount
	         ,bill_to
	         ,comp_code
	         ,daily_monthly_flag
	         ,account_doc_type
	         ,gl_account
) ft
LEFT JOIN
(
	SELECT  billing_doc
	       ,source_id
	       ,CASE WHEN inv_qty_bu is null THEN 0
	             WHEN inv_qty_bu=0 THEN 0  ELSE (SUM(out_thedoor_rev_dc)/inv_qty_bu) END AS unit_price,billing_item
	       ,bill_date
	       ,line_id
	       ,sold_to
	       ,comp_code
	       ,material
	       ,mat_mat_text
	       ,inv_qty_bu
	       ,buom
	       ,profit_ctr
	       ,profit_ctr_desc
	       ,po_date
	       ,sales_order
	       ,po_number
	       ,SUM(net_revenue_dc)                                                          AS extended_price
	       ,SUM(net_revenue_dc)                                                          AS line_amount
	FROM glb_revenue.glb_revenue_vw_tbl
	WHERE source_id='OLYMPUS'
	GROUP BY  1
	         ,2
	         ,4
	         ,5
	         ,6
	         ,7
	         ,8
	         ,9
	         ,10
	         ,11
	         ,12
	         ,13
	         ,14
	         ,15
	         ,16
	         ,17
) rev
ON ft.account_doc =rev.billing_doc AND ft.source_id =rev.source_id
LEFT JOIN
(
	SELECT  source
	       ,billing_doc
	       ,comp_code
	       ,SUM(0) AS tax_amount
	       ,"0INCOTERMS"
	FROM sap_tables.sap_olympus_revenue_data
	GROUP BY  billing_doc
	         ,source
	         ,comp_code
	         ,"0INCOTERMS"
)fr
ON ft.account_doc = fr.billing_doc AND ft.source_id = fr.source AND ft.comp_code = fr.comp_code
WHERE ft.source_id ='OLYMPUS'
AND daily_monthly_flag='D'and account_doc_type IN ('DG', 'AB', 'RV', 'DZ', 'DR', 'SA')
AND ft.gl_account IN ('0000012100', '0000012300')