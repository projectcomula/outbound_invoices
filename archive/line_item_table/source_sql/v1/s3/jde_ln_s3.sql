select 
ft.source_id,
ft.billing_doc as id
,ft.bill_to as customer_id
,ft.comp_code as company_code
,rev.sales_order as order_number
,rev.po_number as po_number
,rev.po_date 
,case 
when ft.bill_type='CO' then 'CM'
when ft.bill_type='RI' then 'INV'
when ft.bill_type='RJ' then 'DM'
when ft.bill_type='RM' then 'CM'
when ft.bill_type='RW' and ft.deb_cre_amt_dc>0 then 'DM'
when ft.bill_type='RW' and ft.deb_cre_amt_dc<0 then 'CM'
else ''
end as "type"
,case 
when ft.source_id='JDE' and glb_cust_mstr.glb_cust_gd.glb_industry_code_1 not in ('DOM','ADOM','PDOM','EC','FGN') then 'YES'
else 'NO'
end as Intercompany_flag
,rev.profit_ctr as lob_code
,rev.profit_ctr_desc as lob_name
,rev.billing_item as line_no
,rev.line_id as line_number
,rev.material as item_code
,rev.mat_mat_text as item_description
,incotx.Sdtax1 as taxable 
,incotx.sdfrth as inco_terms
,rev.buom as item_unit
,rev.inv_qty_bu as line_qty
,rev.unit_price as unit_price
,rev.extended_price as extended_price
,(rev.unit_price*rev.inv_qty_bu) as line_amount
,(ft.tax_amount / 100)  as tax_amount 
,fr.fr_net_revenue_dc as freight_amount
,null as other_1
,null as other_2
from (
select source_id ,billing_doc ,bill_type ,tax_amount,bill_to ,comp_code ,sum(deb_cre_amt_dc) as deb_cre_amt_dc, daily_monthly_flag
from glb_emagia.glb_fiar_ln_emagia  
where source_id ='JDE' and daily_monthly_flag='D' and bill_type in ('CO', 'RI', 'RM', 'RW', 'RJ')
group by source_id ,billing_doc ,bill_type ,tax_amount,bill_to ,comp_code ,daily_monthly_flag
)ft
left join
(
select 
billing_doc,
source_id,
case 
when inv_qty_bu is null then 0
when inv_qty_bu=0 then 0
else (sum(out_thedoor_rev_dc)/inv_qty_bu) 
end as  unit_price,
billing_item
,bill_date
,line_id
,sold_to
,bill_to
,comp_code
,material
,mat_mat_text
,inv_qty_bu
,buom
,profit_ctr
,profit_ctr_desc
,po_date
,sales_order
,po_number
,sum(net_revenue_dc) as extended_price
,sum(net_revenue_dc) as line_amount
from   glb_revenue.glb_revenue_vw_tbl where source_id='JDE' 
group by 1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
)rev 
on
ft.billing_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.billing_item=rev.billing_item
and ft.comp_code=rev.comp_code
and ft.bill_to=rev.bill_to
left join 
(
select 
billing_doc
,source_id
,comp_code 
,typ.bill_type
,sum(fr_net_revenue_dc) as fr_net_revenue_dc
from 
(select 
billing_doc
,source_id
,comp_code 
,bill_type 
,material 
,sum(net_revenue_dc) as fr_net_revenue_dc
from   glb_revenue.glb_revenue_ln where source_id='JDE' and item_type ='F'
--and material in ('36-FREIGHT','DE-FREIGHT','GB-FREIGHT','UK-FREIGHT','491610','491620','491621','491622','491916')
group by billing_doc
,source_id
,comp_code 
,material
,bill_type )typ
 join 
(
select imlitm from  jde_tables.jde_f4101_material where IMSTKT='V' and dl_active_flag=TRUE
)sto
on 
typ.material=sto.imlitm
group by 
billing_doc
,source_id
,comp_code 
,bill_type
)fr
on
ft.billing_doc = fr.billing_doc
and ft.source_id = fr.source_id
and ft.comp_code = fr.comp_code
and ft.bill_type = fr.bill_type
left join (
select
	*
from
	glb_cust_mstr.glb_cust
where 
	 (source_id = 'JDE'	and account_group = 'B')
	)cust 
   on
ft.source_id = cust.source_id
and ft.bill_to = cust.customer_number
left join glb_cust_mstr.glb_cust_gd on
cust.grid_cust_num = glb_cust_mstr.glb_cust_gd.grid_cust_num
left join 
(
 select distinct SDKCO,SDDOC,SDDCTO,SDLNID
   ,sdfrth 
  , Sdtax1  
   from jde_tables.jde_f42119_orditm_close )incotx
   on incotx.SDKCO=ft.comp_code
   and incotx.SDDOC=ft.billing_doc
   and incotx.SDDCTO=ft.bill_type
   --and incotx.SDITM=fiar.item_num
    --and incotx.SDLNID=ft.line_num
where ft.source_id ='JDE' and daily_monthly_flag='D'and ft.bill_type in ('CO', 'RI', 'RM', 'RW', 'RJ')
