SELECT
source_id,
billing_doc,
bill_to,
comp_code,
sales_order,
po_number,
po_date, 
"type",
Intercompany_flag,
profitcenter,
profit_ctr_desc, 
billing_item,
line_id,
material,
mat_mat_text,
taxable, 
inco_terms, 
buom, 
inv_qty_su,
gross_rev_dc,
net_revenue_dc,
line_amount,
tax_amount, 
freight_amount,
other_charges1,
other_charges2
FROM
/*(SELECT
	fiar.source_id,
	fiar.billing_doc,
	fiar.bill_to,
	'0143' AS comp_code,
	--rev.sales_order,
	--rev.po_number,
	--rev.po_date,
	CASE 
		WHEN fiar.bill_type IN ('Payment','Refund','Credit Memo') THEN 'CM'
		WHEN fiar.bill_type = 'Invoice' THEN 'INV'
		WHEN fiar.bill_type = '' THEN ''
		ELSE fiar.bill_type
	END AS "type",
	CASE WHEN customer_posting_group = 'IC' THEN 'YES' ELSE 'NO' END AS Intercompany_flag,
	--rev.profitcenter,
	--rev.profit_ctr_desc,
	--rev.billing_item,
	--rev.line_id,
	--rev.material,
	--rev.mat_mat_text, 
	fiar.taxable,
	fiar.inco_terms,
	--rev.buom,
	--rev.inv_qty_su,
	--rev.out_thedoor_rev_dc/rev.inv_qty_su as gross_rev_dc,
	--rev.net_revenue_dc,
	--(rev.inv_qty_su*rev.buom) as line_amount,
	fiar.tax_amount,
	CASE WHEN fiar.gl_account = '4090' THEN rev.net_revenue_dc ELSE NULL END AS freight_amount,
	null as other_charges1,
	null as other_charges2*/
	
	FROM glb_emagia.glb_fiar_ln_emagia fiar
	/*
	LEFT JOIN 
	(
		SELECT
		billing_doc,
		comp_code,
		sales_order,
		po_number,
		po_date,
		profitcenter,
		profit_ctr_desc,
		billing_item,
		line_id,
		material,
		mat_mat_text,
		buom,
		inv_qty_su,
		sum(out_thedoor_rev_dc) as out_thedoor_rev_dc,
		net_revenue_dc
		FROM glb_revenue.vw_glb_revenue
		WHERE source_id in ('PTMN')
		GROUP BY
		billing_doc,
		comp_code,
		sales_order,
		po_number,
		po_date,
		profitcenter,
		profit_ctr_desc,
		billing_item,
		line_id,
		material,
		mat_mat_text,
		buom,
		inv_qty_su,
		net_revenue_dc
	) rev
	--ON fiar.billing_doc = rev.billing_doc
	--AND fiar.comp_code = rev.comp_code 

	
	WHERE fiar.source_id = 'PTMN'
) LNT  
GROUP BY
source_id,
billing_doc,
bill_to,
comp_code,
sales_order,
po_number,
po_date,
"type",
Intercompany_flag,
profitcenter,
profit_ctr_desc,
billing_item,
line_id,
material,
mat_mat_text,
taxable,
inco_terms,
buom,
inv_qty_su,
gross_rev_dc,
net_revenue_dc,
line_amount,
tax_amount,
freight_amount,
other_charges1,
other_charges2
*/