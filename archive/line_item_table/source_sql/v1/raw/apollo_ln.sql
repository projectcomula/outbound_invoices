select * from  public.glb_ar_col_emagia_ln_itm where source_id ='OLYMPUS' limit 100

delete public.glb_ar_col_emagia_ln_itm
--insert into public.glb_ar_col_emagia_ln_itm
--select count(*) from  (
select 
ft.source_id,
ft.account_doc as id
,ft.bill_to as customer_id
,ft.comp_code as company_code
,rev.sales_order as order_number
,rev.po_number as po_number
,rev.po_date 
,case 
when account_doc_type='DG' then 'CM'
when account_doc_type ='AB' and  deb_cre_amt_dc<0  then 'CM'
when account_doc_type='AB' and deb_cre_amt_dc>=0  then 'DM'
when account_doc_type='RV' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='RV' and  deb_cre_amt_dc>=0  then 'INV' 
when account_doc_type='DZ' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='DZ' and  deb_cre_amt_dc>=0  then 'INV'
when account_doc_type='DR'  then 'INV'
when account_doc_type='DA' then 'INV'
else ''
end as "type"
,case 
when ft .source_id='APOLLO' and (cust_gd.glb_industry_code_1_description='ICO' OR cred_rep_group='029') then 'YES' 
else 'NO'
end as Intercompany_flag
,rev.profit_ctr as lob_code
,rev.profit_ctr_desc as lob_name
,rev.billing_item as line_no
,rev.line_id as line_number
,rev.material as item_code
,rev.mat_mat_text as item_description
,null as taxable 
,null as inco_terms
,rev.buom as item_unit
,rev.inv_qty_bu as line_qty
,rev.unit_price as unit_price
,rev.extended_price as extended_price
,(rev.unit_price*rev.inv_qty_bu) as line_amount
,(ft.tax_amount )  as tax_amount 
,null as freight_amount
,null as other_1
,null as other_2
from public.glb_fiar_ln_emagia  ft
--where ft.source_id ='JDE' and daily_monthly_flag='D'


left join
(
select 
billing_doc,
source_id,
case 
when inv_qty_bu is null then 0
when inv_qty_bu=0 then 0
else (sum(out_thedoor_rev_dc)/inv_qty_bu) 
end as  unit_price,
billing_item
,bill_date
,line_id
,sold_to
,comp_code
,material
,mat_mat_text
,inv_qty_bu
,buom
,profit_ctr
,profit_ctr_desc
,po_date
,sales_order
,po_number
,sum(net_revenue_dc) as extended_price
,sum(net_revenue_dc) as line_amount
from   glb_revenue.glb_revenue_vw_tbl where source_id='APOLLO' 
group by 1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17
)rev 
on
ft.account_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.billing_item=rev.billing_item
and ft.comp_code=rev.comp_code
--and ft.bill_to=rev.bill_to
/*left join 
(
select 
billing_doc
,source_id
,comp_code 
,typ.bill_type
,sum(fr_net_revenue_dc) as fr_net_revenue_dc
from 
(select 
billing_doc
,source_id
,comp_code 
,bill_type 
,billing_item 
,sum(net_revenue_dc) as fr_net_revenue_dc
from   glb_revenue.glb_revenue_ln where source_id='APOLLO' and salesitem_type ='F'
--and material in ('36-FREIGHT','DE-FREIGHT','GB-FREIGHT','UK-FREIGHT','491610','491620','491621','491622','491916')
group by billing_doc
,source_id
,comp_code 
,billing_item
,bill_type )typ
 /*join 
(
select imitm from  jde_tables.jde_f4101_material where IMSTKT='V' and dl_active_flag=TRUE
)sto
on 
typ.billing_item=sto.imitm*/
group by 
billing_doc
,source_id
,comp_code 
,bill_type
)fr
on
ft.billing_doc = fr.billing_doc
and ft.source_id = fr.source_id
and ft.comp_code = fr.comp_code
and ft.bill_type = fr.bill_type*/
LEFT JOIN (select *
   from GLB_CUST_MSTR.glb_cust where (source_id in ('APOLLO')) 
                                      
									  )cust --subsq1
   ON ft.source_id = cust.source_id and ft.bill_to = cust.customer_number
   
   LEFT JOIN glb_cust_mstr.glb_cust_gd cust_gd on cust.grid_cust_num = cust_gd.grid_cust_num


where ft.source_id ='APOLLO' and daily_monthly_flag='D'and account_doc_type in ('DG','AB','RV','DZ','DR','DA')
and ft.comp_code in ('0120','0150') and gl_account in ('0000012100','0000012920')
)
