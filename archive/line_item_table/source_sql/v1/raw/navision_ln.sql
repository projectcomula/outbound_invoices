INSERT INTO public.glb_ar_col_emagia_ln_itm

SELECT
source_id,
billing_doc,
bill_to,
comp_code,
sales_order,
po_number,
po_date,
"type",
Intercompany_flag,
profitcenter,
profit_ctr_desc,
billing_item,
line_id,
material,
mat_mat_text,
taxable,
inco_terms,
suom,
inv_qty_su,
unit_price,
extended_price,
line_amount,
tax_amount,
freight_amount,
other_charges1,
other_charges2
FROM
(
	SELECT
	fiar.source_id,
	fiar.billing_doc,
	fiar.bill_to,
	fiar.comp_code,
	rev.sales_order,
	rev.po_number,
	rev.po_date,
	CASE 
		WHEN fiar.bill_type IN ('Payment','Refund','Credit Memo') THEN 'CM'
		WHEN fiar.bill_type = 'Invoice' THEN 'INV'
		WHEN fiar.bill_type = '' THEN ''
		ELSE fiar.bill_type
	END AS "type",
	CASE WHEN customer_posting_group = 'IC' THEN 'YES' ELSE 'NO' END AS Intercompany_flag,
	rev.profitcenter,
	rev.profit_ctr_desc,
	rev.billing_item,
	rev.line_id,
	rev.material,
	rev.mat_mat_text,
	fiar.taxable,
	fiar.inco_terms,
	rev.suom,
	rev.inv_qty_su,
	CASE WHEN rev.inv_qty_su != 0 THEN fiar.deb_cre_amt_dc/rev.inv_qty_su ELSE fiar.deb_cre_amt_dc END AS unit_price,
	0 AS extended_price,
	fiar.deb_cre_amt_dc AS line_amount,
	fiar.tax_amount,
	fiar.freight_amount,
	null as other_charges1,
	null as other_charges2
	
	FROM public.glb_fiar_ln_emagia fiar
	
	LEFT JOIN 
	(
		SELECT
		billing_doc,
		comp_code,
		sales_order,
		po_number,
		po_date,
		profitcenter,
		profit_ctr_desc,
		billing_item,
		line_id,
		material,
		mat_mat_text,
		suom,
		inv_qty_su
		FROM glb_revenue.vw_glb_revenue
		WHERE source_id in ('MIM','IMT','ACUTRONIC')
		GROUP BY
		billing_doc,
		comp_code,
		sales_order,
		po_number,
		po_date,
		profitcenter,
		profit_ctr_desc,
		billing_item,
		line_id,
		material,
		mat_mat_text,
		suom,
		inv_qty_su
	) rev
	ON fiar.billing_doc = rev.billing_doc
	AND fiar.comp_code = rev.comp_code
	
	WHERE fiar.source_id IN ('MAJESTY','IMT','ARC')
) LNT
GROUP BY
source_id,
billing_doc,
bill_to,
comp_code,
sales_order,
po_number,
po_date,
"type",
Intercompany_flag,
profitcenter,
profit_ctr_desc,
billing_item,
line_id,
material,
mat_mat_text,
taxable,
inco_terms,
suom,
inv_qty_su,
unit_price,
extended_price,
line_amount,
tax_amount,
freight_amount,
other_charges1,
other_charges2
;