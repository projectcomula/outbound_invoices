select * from  public.glb_ar_col_emagia_ln_itm where source_id ='OLYMPUS' limit 100

delete public.glb_ar_col_emagia_ln_itm where source_id ='OLYMPUS'
--insert into public.glb_ar_col_emagia_ln_itm
--select count(*) from  (
select 
ft.source_id,
ft.account_doc as id
,ft.bill_to as customer_id
,ft.comp_code as company_code
,rev.sales_order as order_number
,rev.po_number as po_number
,rev.po_date 
,case 
when account_doc_type='DG' then 'CM'
when account_doc_type ='AB' and  deb_cre_amt_dc<0  then 'CM'
when account_doc_type='AB' and deb_cre_amt_dc>=0  then 'INV'
when account_doc_type='RV' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='RV' and  deb_cre_amt_dc>=0  then 'INV' 
when account_doc_type='DZ' then 'CM'
when account_doc_type='DR'  then 'INV'
when account_doc_type='SA' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='SA' and  deb_cre_amt_dc>=0  then 'DM' 
else ''
end as "type"
,case 
when ft.source_id='OLYMPUS' and  ft.gl_account='0000012300' then 'YES'
else 'NO'
end as Intercompany_flag
,rev.profit_ctr as lob_code
,rev.profit_ctr_desc as lob_name
,rev.billing_item as line_no
,rev.line_id as line_number
,rev.material as item_code
,rev.mat_mat_text as item_description
,ta.YMWSKZ as taxable 
,"0INCOTERMS" as inco_terms
,rev.buom as item_unit
,rev.inv_qty_bu as line_qty
,rev.unit_price as unit_price
,rev.extended_price as extended_price
,(rev.unit_price*rev.inv_qty_bu) as line_amount
,(fr.tax_amount ) as tax_amount
,null as freight_amount
,null as other_1
,null as other_2
from (
select source_id ,account_doc  ,tax_amount,bill_to ,comp_code ,sum(deb_cre_amt_dc) as deb_cre_amt_dc, daily_monthly_flag,account_doc_type,gl_account
from public.glb_fiar_ln_emagia  
where source_id ='OLYMPUS' and daily_monthly_flag='D' and account_doc_type in ('DG','AB','RV','DZ','DR','SA')
and gl_account in ('0000012100','0000012300')
group by source_id ,account_doc  ,tax_amount,bill_to ,comp_code ,daily_monthly_flag,account_doc_type,gl_account
)  ft
--where ft.source_id ='JDE' and daily_monthly_flag='D'


left join
(
select 
billing_doc,
source_id,
case 
when inv_qty_bu is null then 0
when inv_qty_bu=0 then 0
else (sum(out_thedoor_rev_dc)/inv_qty_bu) 
end as  unit_price,
billing_item
,bill_date
,line_id
,sold_to
,comp_code
,material
,mat_mat_text
,inv_qty_bu
,buom
,profit_ctr
,profit_ctr_desc
,po_date
,sales_order
,po_number
,sum(net_revenue_dc) as extended_price
,sum(net_revenue_dc) as line_amount
from   glb_revenue.glb_revenue_vw_tbl where source_id='OLYMPUS' 
group by 1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17
)rev 
on
ft.account_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.billing_item=rev.billing_item
and ft.comp_code=rev.comp_code
--and ft.bill_to=rev.bill_to
left join 
(
select
	source,
	billing_doc,
	comp_code,
	sum("0TAX_AMOUNT") as tax_amount,
	"0INCOTERMS"

from
	sap_tables.sap_olympus_revenue_data 
group by
	billing_doc
,
	source
,
	comp_code 
,
	"0INCOTERMS"

)fr
on
ft.account_doc = fr.billing_doc
and ft.source_id = fr.source
and ft.comp_code = fr.comp_code
--and ft.account_doc_type = fr.billing_type
left join 
(
select distinct  YMWSKZ,"0ac_doc_no" ,"0ac_doc_typ" ,"0comp_code" from sap_tables.sap_olympus_fiar_line_item 
) ta 
on 
ft.account_doc =ta."0ac_doc_no" 

and ft.account_doc_type=ta."0ac_doc_typ"
and ft.comp_code=ta."0comp_code"
where ft.source_id ='OLYMPUS' and daily_monthly_flag='D'and account_doc_type in ('DG','AB','RV','DZ','DR','SA')
and ft.gl_account in ('0000012100','0000012300')
)