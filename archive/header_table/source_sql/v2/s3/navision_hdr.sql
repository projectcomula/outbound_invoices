SELECT
unique_id AS unique_id,
number AS number,
null AS trx_number,
null AS id_year,
comp_code AS company_code,
type AS atype,
null AS order_type,
bill_date AS trx_date,
net_due_date AS trx_due_date,
amount AS amount,
deb_cre_amt_lc AS inv_amount,
sold_to AS sold_to_customer_id,
bill_to AS bill_to_customer_id,
invc_pmt_terms AS term,
null AS po_number,
null AS po_date,
null AS so_number,
null AS so_date,
null AS internal_notes,
m_rate_curr_lc AS exchange_rate,
doc_curr AS invoice_curr_code,
complete_flag AS complete_flag,
unpaid_amt AS over_due_amount,
unpaid_amt_lc AS func_over_due_amount,
curr_due_amount AS curr_due_amount,
null AS last_paid_amount,
func_curr_due_amount AS func_curr_due_amount,
null AS last_paid_date,
null AS check_number,
null AS delivery_note_number,
null AS rma_numbers,
null AS related_cust_trx_id,
null AS related_trx_year,
null AS related_cust_trx_date,
null AS ship_to_contact_id,
null AS bill_to_contact_id,
null AS lob_code,
null AS sold_to_site_use_id,
null AS bill_to_site_use_id,
null AS primary_salesrep_id,
null AS outside_salesrep_id,
tax_amount AS tax_amount,
freight_amount AS freight_amount,
null AS other_charges1,
null AS other_charges2,
reversed AS reversed,
null AS lob_name,
customer_region AS region,
null AS project_number,
null AS contract_number,
null AS contract_date,
null AS ship_via,
null AS ship_ware_house,
null AS reversal_gl_date,
revenue_amount AS revenue_amount,
null AS func_revenue_amt,
null AS original_trx_type,
posting_date AS gl_date,
deb_cre_amt_gc AS cons_amount,
unpaid_amt_gc AS cons_over_due_amount,
cons_curr_due_amount AS cons_curr_due_amount,
null AS cons_last_paid_amt,
null AS cons_revenue_amt,
m_rate_curr_gc AS cons_exchange_rate,
null AS customer_service_rep_id,
null AS tracking_no,
null AS invoice_header_text,
null AS courier_company_code,
Intercompany_flag AS intercompany_flag,
source_id AS source_id,
null AS remark,
null AS payer_customer_id,
null AS invoice_status_code,
CURRENT_DATE AS datekey
FROM
(
	SELECT
	source_id,
	unique_id,
	number,
	comp_code,
	sold_to,
	bill_to,
	bill_date,
	net_due_date,
	invc_pmt_terms,
	customer_region,
	Intercompany_flag,
	complete_flag,
	posting_date,
	reversed,
	m_rate_curr_lc,
	"atype",
	doc_curr,
	deb_cre_amt_lc,
	amount,
	revenue_amount,
	tax_amount,
	freight_amount,
	unpaid_amt,
	unpaid_amt_lc,
	curr_due_amount,
	func_curr_due_amount,
	deb_cre_amt_gc,
	unpaid_amt_gc,
	cons_curr_due_amount,
	m_rate_curr_gc
	FROM
	(
		SELECT
		fiar.source_id,
		fiar.billing_doc as unique_id,
		fiar.billing_doc as number,
		fiar.comp_code,
		rev.sold_to,
		fiar.bill_to,
		fiar.bill_date,
		fiar.net_due_date,
		fiar.invc_pmt_terms,
		cntry.customer_region,
		CASE 
			WHEN fiar.customer_posting_group = 'IC' THEN 'YES' 
			ELSE 'NO' 
		END AS Intercompany_flag,
		CASE 
			WHEN unpaid_amt_gc = 0 THEN 'Y' 
			WHEN unpaid_amt_gc = deb_cre_amt_gc THEN 'N' 
			WHEN unpaid_amt_gc < deb_cre_amt_gc AND unpaid_amt_gc > 0 THEN 'P' 
		END AS complete_flag,
		fiar.posting_date,
		'N' AS reversed,
		fiar.m_rate_curr_lc,
		CASE 
			WHEN fiar.bill_type IN ('Payment','Refund','Credit Memo') THEN 'CM'
			WHEN fiar.bill_type = 'Invoice' THEN 'INV'
			WHEN fiar.bill_type = '' THEN ''
			ELSE fiar.bill_type
		END AS "atype",
		fiar.doc_curr,
		fiar.deb_cre_amt_lc,
		fiar.deb_cre_amt_dc as amount,
		fiar.deb_cre_amt_dc as revenue_amount,
		fiar.tax_amount,
		fiar.freight_amount,
		fiar.unpaid_amt,
		fiar.unpaid_amt_lc,
		case when net_due_date>CURRENT_DATE then  unpaid_amt else 0  end as curr_due_amount,
		case when net_due_date<CURRENT_DATE then  unpaid_amt_lc else 0 end as func_curr_due_amount,
		fiar.deb_cre_amt_gc,
		fiar.unpaid_amt_gc,
		case when net_due_date>CURRENT_DATE then  unpaid_amt_gc else 0 end as cons_curr_due_amount,
		fiar.m_rate_curr_gc
		
		FROM glb_emagia.glb_fiar_ln_emagia AS fiar
		
		LEFT JOIN 
		(
			SELECT
			sold_to,
			net_revenue_dc,
			billing_doc,
			comp_code
			FROM glb_revenue.vw_glb_revenue
			WHERE source_id in ('MIM','IMT','ACUTRONIC')
			GROUP BY
			sold_to,
			net_revenue_dc,
			billing_doc,
			comp_code
		) rev
		ON fiar.billing_doc = rev.billing_doc
		AND fiar.comp_code = rev.comp_code
		
		LEFT JOIN 
		(
			SELECT 
			region2 AS customer_region,
			ctry_code
			FROM glb_lkp.country_master
			GROUP BY 
			region2,
			ctry_code
		) cntry
		ON fiar.country_cd = cntry.ctry_code
		
		WHERE fiar.source_id IN ('MAJESTY','IMT','ARC')
	) HDR
	GROUP BY
	source_id,
	unique_id,
	number,
	comp_code,
	sold_to,
	bill_to,
	bill_date,
	net_due_date,
	invc_pmt_terms,
	customer_region,
	Intercompany_flag,
	complete_flag,
	posting_date,
	reversed,
	m_rate_curr_lc,
	"atype",
	doc_curr,
	deb_cre_amt_lc,
	amount,
	revenue_amount,
	tax_amount,
	freight_amount,
	unpaid_amt,
	unpaid_amt_lc,
	curr_due_amount,
	func_curr_due_amount,
	deb_cre_amt_gc,
	unpaid_amt_gc,
	cons_curr_due_amount,
	m_rate_curr_gc
) NVSN_HDR