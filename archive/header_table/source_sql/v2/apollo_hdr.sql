SELECT
unique_id AS unique_id,
number AS number,
null AS trx_number,
null AS id_year,
company_code AS company_code,
type AS type,
null AS order_type,
trx_date AS trx_date,
trx_due_date AS trx_due_date,
amount AS amount,
inv_amout AS inv_amount,
sold_to_customer_id AS sold_to_customer_id,
bill_to_customer_id AS bill_to_customer_id,
term AS term,
null AS po_number,
null AS po_date,
null AS so_number,
null AS so_date,
null AS internal_notes,
exchange_rate AS exchange_rate,
invoice_curr_code AS invoice_curr_code,
complete_flag AS complete_flag,
over_due_amount AS over_due_amount,
func_over_due_amount AS func_over_due_amount,
cur_due_amount AS curr_due_amount,
null AS last_paid_amount,
func_curr_due_amount AS func_curr_due_amount,
null AS last_paid_date,
null AS check_number,
null AS delivery_note_number,
null AS rma_numbers,
null AS related_cust_trx_id,
null AS related_trx_year,
null AS related_cust_trx_date,
null AS ship_to_contact_id,
null AS bill_to_contact_id,
null AS lob_code,
null AS sold_to_site_use_id,
null AS bill_to_site_use_id,
null AS primary_salesrep_id,
null AS outside_salesrep_id,
tax_amount AS tax_amount,
freight_amount AS freight_amount,
null AS other_charges1,
null AS other_charges2,
reserved AS reversed,
null AS lob_name,
region AS region,
null AS project_number,
null AS contract_number,
null AS contract_date,
null AS ship_via,
null AS ship_ware_house,
null AS reversal_gl_date,
revenue_amount AS revenue_amount,
null AS func_revenue_amt,
null AS original_trx_type,
gl_date AS gl_date,
cons_amount AS cons_amount,
cons_over_due_amount AS cons_over_due_amount,
cons_curr_due_amount AS cons_curr_due_amount,
null AS cons_last_paid_amt,
null AS cons_revenue_amt,
cons_exchange_rate AS cons_exchange_rate,
null AS customer_service_rep_id,
null AS tracking_no,
null AS invoice_header_text,
null AS courier_company_code,
Intercompany_flag AS intercompany_flag,
source_id AS source_id,
null AS remark,
payer AS payer_customer_id,
null AS invoice_status_code,
null AS datekey
FROM
(
	select 
	ft.source_id,
	ft.account_doc as unique_id
	,ft.account_doc as number 
	,ft.comp_code as company_code
	,rev.sold_to as sold_to_customer_id
	,ft.bill_to as bill_to_customer_id
	,ft.bill_date as trx_date
	,net_due_date as trx_due_date
	,invc_pmt_terms as term 
	,cust_region.customer_region as region
	,case 
	when ft .source_id='APOLLO' and (cust_gd.glb_industry_code_1_description='ICO' OR cred_rep_group='029') then 'YES' 
	else 'NO'
	end as Intercompany_flag
	,case 
	when unpaid_amt_gc =0 then 'Y'
	when unpaid_amt_gc =deb_cre_amt_gc then 'N'
	when unpaid_amt_gc<deb_cre_amt_gc and unpaid_amt_gc>0 then 'P'
	else null
	end as complete_flag
	,posting_date as gl_date
	,'N' as reserved
	,m_rate_curr_lc as exchange_rate
	,case 
	when account_doc_type='DG' then 'CM'
	when account_doc_type ='AB' and  deb_cre_amt_dc<0  then 'CM'
	when account_doc_type='AB' and deb_cre_amt_dc>=0  then 'DM'
	when account_doc_type='RV' and  deb_cre_amt_dc<0  then 'CM' 
	when account_doc_type='RV' and  deb_cre_amt_dc>=0  then 'INV' 
	when account_doc_type='DZ' and  deb_cre_amt_dc<0  then 'CM' 
	when account_doc_type='DZ' and  deb_cre_amt_dc>=0  then 'INV'
	when account_doc_type='DR'  then 'INV'
	when account_doc_type='DA' then 'INV'
	else ''
	end as type
	,doc_curr as invoice_curr_code
	,sum(deb_cre_amt_lc) as inv_amout
	,sum(deb_cre_amt_dc) as amount 
	,sum(deb_cre_amt_dc) as revenue_amount
	,null as tax_amount 
	,sum(fr.freight) as freight_amount 
	,sum(unpaid_amt) as over_due_amount
	,sum(unpaid_amt_lc) as func_over_due_amount
	,case 
	when net_due_date>CURRENT_DATE then  sum(unpaid_amt)
	else 0
	end as  cur_due_amount
	,case 
	when net_due_date<CURRENT_DATE then  sum(unpaid_amt_lc)
	else 0
	end as func_curr_due_amount
	,sum(deb_cre_amt_gc) as cons_amount
	,sum(unpaid_amt_gc) as cons_over_due_amount
	,case 
	when net_due_date>CURRENT_DATE then  sum(unpaid_amt_gc)
	else 0
	end as cons_curr_due_amount 
	,m_rate_curr_gc as cons_exchange_rate
	,null
	,fr.payer
	,null
	,current_date as datekey
	from (
	select  source_id ,account_doc,comp_code,doc_curr,net_due_date,posting_date,bill_to, sum(deb_cre_amt_dc)as deb_cre_amt_dc,sum(deb_cre_amt_lc) as deb_cre_amt_lc,account_doc_type,invc_pmt_terms,
	bill_date,sum(unpaid_amt) as unpaid_amt,invc_status_cd,sum(deb_cre_amt_gc)as deb_cre_amt_gc,m_rate_curr_lc,m_rate_curr_gc,sum(unpaid_amt_lc) as unpaid_amt_lc,
	sum(unpaid_amt_gc) as unpaid_amt_gc ,sum(tax_amount) as tax_amount,daily_monthly_flag,gl_account,cred_rep_group
	from emagia_final where source_id = 'APOLLO'
	and daily_monthly_flag = 'D'
	and account_doc_type in ('DG','AB','RV','DZ','DR','DA')
	and comp_code in ('0120','0150') and gl_account in ('0000012100','0000012920')  group by  source_id ,account_doc,comp_code,doc_curr,net_due_date,
	posting_date,bill_to,account_doc_type,invc_pmt_terms,bill_date,invc_status_cd,m_rate_curr_lc,m_rate_curr_gc,daily_monthly_flag,gl_account,cred_rep_group
	)  ft
	left join 
	(
	select distinct
	source_id ,
	customer_number,
	customer_region, 
	customer_reporting_country 
	from glb_cust where source_id ='APOLLO'
	)cust_region
	on ft.bill_to =cust_region.customer_number 
	and ft.source_id =cust_region.source_id 
	left join
	(
	select 
	billing_doc,
	source_id,
	sold_to,
	comp_code
	from   glb_revenue where source_id='APOLLO' 
	group by 1,2,3,4
	)rev 
	on
	ft.account_doc =rev.billing_doc 
	and ft.source_id =rev.source_id 
	and  ft.comp_code=rev.comp_code
	left join 
	(
	select source,billing_doc,comp_code,payer,sum(subtotal_4) as freight from sap_tables.sap_apollo_revenue_data
	group by billing_doc
	,source
	,comp_code 
	,payer)fr
	on
	ft.account_doc = fr.billing_doc
	and ft.source_id = fr.source
	and ft.comp_code = fr.comp_code
	LEFT JOIN (select *
	from glb_cust where (source_id in ('APOLLO')))cust 
	ON ft.source_id = cust.source_id and ft.bill_to = cust.customer_number
	LEFT JOIN glb_cust_gd cust_gd on cust.grid_cust_num = cust_gd.grid_cust_num
	where
	ft.source_id = 'APOLLO'
	and daily_monthly_flag = 'D'
	and account_doc_type in ('DG','AB','RV','DZ','DR','DA')
	and ft.comp_code in ('0120','0150') and gl_account in ('0000012100','0000012920')
	group by
	ft.source_id
	,number
	,company_code
	,sold_to_customer_id
	,bill_to_customer_id
	,trx_date
	,trx_due_date
	,term
	,region
	,Intercompany_flag
	,complete_flag
	,gl_date
	,reserved
	,exchange_rate
	,type
	,invoice_curr_code
	,cons_exchange_rate
	,fr.payer
) APLO_HDR