select * from public.glb_ar_col_emagia_hdr

delete  public.glb_ar_col_emagia_hdr where source_id ='APOLLO'
--insert into public.glb_ar_col_emagia_hdr
--select count(*)  from (
select 
ft.source_id,
ft.account_doc as unique_id
,ft.account_doc as number 
,ft.comp_code as company_code
,rev.sold_to as sold_to_customer_id
,ft.bill_to as bill_to_customer_id
,ft.bill_date as trx_date
,net_due_date as trx_due_date
,invc_pmt_terms as term 
,cust_region.customer_region as region
,case 
when ft .source_id='APOLLO' and (cust_gd.glb_industry_code_1_description='ICO' OR cred_rep_group='029') then 'YES' 
else 'NO'
end as Intercompany_flag
,case 
when unpaid_amt_gc =0 then 'Y'
when unpaid_amt_gc =deb_cre_amt_gc then 'N'
when unpaid_amt_gc<deb_cre_amt_gc and unpaid_amt_gc>0 then 'P'
else null
end as complete_flag
,posting_date as gl_date
,'N' as reserved
,m_rate_curr_lc as exchange_rate
,case 
when account_doc_type='DG' then 'CM'
when account_doc_type ='AB' and  deb_cre_amt_dc<0  then 'CM'
when account_doc_type='AB' and deb_cre_amt_dc>=0  then 'DM'
when account_doc_type='RV' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='RV' and  deb_cre_amt_dc>=0  then 'INV' 
when account_doc_type='DZ' and  deb_cre_amt_dc<0  then 'CM' 
when account_doc_type='DZ' and  deb_cre_amt_dc>=0  then 'INV'
when account_doc_type='DR'  then 'INV'
when account_doc_type='DA' then 'INV'
else ''
end as "type"
,doc_curr as invoice_curr_code
,sum(deb_cre_amt_lc) as inv_amout
,sum(deb_cre_amt_dc) as amount 
,sum(deb_cre_amt_dc) as revenue_amount
,null as tax_amount 
,null as freight_amount 
,sum(unpaid_amt) as over_due_amount
,sum(unpaid_amt_lc) as func_over_due_amount
,case 
when net_due_date>CURRENT_DATE then  sum(unpaid_amt)
else 0
end as  cur_due_amount
,case 
when net_due_date<CURRENT_DATE then  sum(unpaid_amt_lc)
else 0
end as func_curr_due_amount
,sum(deb_cre_amt_gc) as cons_amount
,sum(unpaid_amt_gc) as cons_over_due_amount
,case 
when net_due_date>CURRENT_DATE then  sum(unpaid_amt_gc)
else 0
end as cons_curr_due_amount 
,m_rate_curr_gc as cons_exchange_rate
from public.glb_fiar_ln_emagia  ft
--where ft.source_id ='JDE' and daily_monthly_flag='D'

left join 
(
select distinct
source_id ,
customer_number,
customer_region, 
customer_reporting_country 
from glb_cust_mstr.glb_cust where source_id ='APOLLO'
)cust_region
on ft.bill_to =cust_region.customer_number 
and ft.source_id =cust_region.source_id 

left join
(
select 
billing_doc,
source_id,
--bill_date,
sold_to,
comp_code

from   glb_revenue.glb_revenue_ln where source_id='APOLLO'
group by 1,2,3,4
)rev 
on
ft.account_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.bill_date=rev.bill_date
--and ft.bill_to=rev.bill_to
and  ft.comp_code=rev.comp_code
/*
left join 
(
select 
billing_doc
,source_id
,comp_code 
,typ.bill_type
,sum(fr_net_revenue_dc) as fr_net_revenue_dc
from 
(select 
billing_doc
,source_id
,comp_code 
,bill_type 
,billing_item 
,sum(net_revenue_dc) as fr_net_revenue_dc
from   glb_revenue.glb_revenue_ln where source_id='APOLLO' and salesitem_type ='F'
--and material in ('36-FREIGHT','DE-FREIGHT','GB-FREIGHT','UK-FREIGHT','491610','491620','491621','491622','491916')
group by billing_doc
,source_id
,comp_code 
,billing_item
,bill_type )typ
 /*join 
(
select imitm from  jde_tables.jde_f4101_material where IMSTKT='V' and dl_active_flag=TRUE
)sto
on 
typ.billing_item=sto.imitm*/
group by 
billing_doc
,source_id
,comp_code 
,bill_type
)fr
on
ft.account_doc = fr.billing_doc
and ft.source_id = fr.source_id
and ft.comp_code = fr.comp_code
and ft.account_doc_type = fr.bill_type
*/
LEFT JOIN (select *
   from GLB_CUST_MSTR.glb_cust where (source_id in ('APOLLO')) 
                                      
									  )cust --subsq1
   ON ft.source_id = cust.source_id and ft.bill_to = cust.customer_number
   
   LEFT JOIN glb_cust_mstr.glb_cust_gd cust_gd on cust.grid_cust_num = cust_gd.grid_cust_num
where
ft.source_id = 'APOLLO'
and daily_monthly_flag = 'D'
and account_doc_type in ('DG','AB','RV','DZ','DR','DA')
and ft.comp_code in ('0120','0150') and gl_account in ('0000012100','0000012920')
group by
ft.source_id
,"number"
,company_code
,sold_to_customer_id
,bill_to_customer_id
,trx_date
,trx_due_date
,term
,region
,Intercompany_flag
,complete_flag
,gl_date
,reserved
,exchange_rate
,"type"
,invoice_curr_code
,cons_exchange_rate

)