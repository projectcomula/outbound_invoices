INSERT INTO public.glb_ar_col_emagia_hdr

SELECT
source_id,
unique_id,
number,
comp_code,
sold_to,
bill_to,
bill_date,
net_due_date,
invc_pmt_terms,
customer_region,
Intercompany_flag,
complete_flag,
posting_date, 
reversed,
m_rate_curr_lc,
"type",
doc_curr,
deb_cre_amt_lc,
amount,
revenue_amount,
tax_amount, 
net_revenue_dc,
unpaid_amt,
unpaid_amt_lc,
curr_due_amount,
func_curr_due_amount,
deb_cre_amt_gc,
unpaid_amt_gc,
cons_curr_due_amount,
m_rate_curr_gc
FROM
(
	SELECT
	fiar.source_id,
	fiar.billing_doc as unique_id,
	fiar.billing_doc as number,
	'0143' AS comp_code,
	rev.sold_to,
	fiar.bill_to,
	fiar.bill_date,
	fiar.net_due_date,
	fiar.invc_pmt_terms,
	cust.customer_region,
	CASE 
		WHEN fiar.customer_posting_group = 'IC' THEN 'YES' 
		ELSE 'NO' 
	END AS Intercompany_flag,
	CASE 
		WHEN unpaid_amt_gc = 0 THEN 'Y' 
		WHEN unpaid_amt_gc = deb_cre_amt_gc THEN 'N' 
		WHEN unpaid_amt_gc < deb_cre_amt_gc AND unpaid_amt_gc > 0 THEN 'P' 
	END AS complete_flag,
	fiar.posting_date,
	'N' AS reversed,
	fiar.m_rate_curr_lc,
	CASE 
		WHEN fiar.bill_type IN ('Payment','Refund','Credit Memo') THEN 'CM'
		WHEN fiar.bill_type = 'Invoice' THEN 'INV'
		WHEN fiar.bill_type = '' THEN ''
		ELSE fiar.bill_type
	END AS "type",
	fiar.doc_curr,
	fiar.deb_cre_amt_lc,
	fiar.deb_cre_amt_dc as amount,
	fiar.deb_cre_amt_dc as revenue_amount,
	fiar.tax_amount,
	rev.net_revenue_dc,
	fiar.unpaid_amt,
	fiar.unpaid_amt_lc,
	case when net_due_date>CURRENT_DATE then  unpaid_amt else 0  end as curr_due_amount,
	case when net_due_date<CURRENT_DATE then  unpaid_amt_lc else 0 end as func_curr_due_amount,
	fiar.deb_cre_amt_gc,
	fiar.unpaid_amt_gc,
	case when net_due_date>CURRENT_DATE then  unpaid_amt_gc else 0 end as cons_curr_due_amount,
	fiar.m_rate_curr_gc
	
	FROM public.glb_fiar_ln_emagia AS fiar
	
	LEFT JOIN 
	(
		SELECT
		sold_to,
		net_revenue_dc,
		billing_doc,
		comp_code
		FROM glb_revenue.vw_glb_revenue
		WHERE source_id in ('PTMN')
		GROUP BY
		sold_to,
		net_revenue_dc,
		billing_doc,
		comp_code
	) rev
	ON fiar.billing_doc = rev.billing_doc
	-- AND fiar.comp_code = rev.comp_code
	
	LEFT JOIN 
	(
		SELECT 
		customer_region,
		customer_number
		FROM glb_cust_mstr.glb_cust
		WHERE source_id in ('PTMN')
		GROUP BY 
		customer_region,
		customer_number
	) cust
	ON fiar.bill_to = cust.customer_number
	
	WHERE fiar.source_id = 'PTMN'
) HDR
GROUP BY
source_id,
unique_id,
number,
comp_code,
sold_to,
bill_to,
bill_date,
net_due_date,
invc_pmt_terms,
customer_region,
Intercompany_flag,
complete_flag,
posting_date,
reversed,
m_rate_curr_lc,
"type",
doc_curr,
deb_cre_amt_lc,
amount,
revenue_amount,
tax_amount,
net_revenue_dc,
unpaid_amt,
unpaid_amt_lc,
curr_due_amount,
func_curr_due_amount,
deb_cre_amt_gc,
unpaid_amt_gc,
cons_curr_due_amount,
m_rate_curr_gc
;