SELECT  ft.source_id
       ,ft.account_doc                                                                     AS unique_id
       ,ft.account_doc                                                                     AS number
       ,ft.comp_code                                                                       AS company_code
       ,rev.sold_to                                                                        AS sold_to_customer_id
       ,ft.bill_to                                                                         AS bill_to_customer_id
       ,ft.bill_date                                                                       AS trx_date
       ,net_due_date                                                                       AS trx_due_date
       ,invc_pmt_terms                                                                     AS term
       ,cust_region.customer_region                                                        AS region
       ,CASE WHEN ft .source_id='APOLLO' AND (glb_cust_mstr.glb_cust_gd.glb_industry_code_1_description='ICO' OR cred_rep_group='029') THEN 'YES'  ELSE 'NO' END AS Intercompany_flag
       ,CASE WHEN unpaid_amt_gc =0 THEN 'Y'
             WHEN unpaid_amt_gc =deb_cre_amt_gc THEN 'N'
             WHEN unpaid_amt_gc<deb_cre_amt_gc AND unpaid_amt_gc>0 THEN 'P'  ELSE null END AS complete_flag
       ,posting_date                                                                       AS gl_date
       ,'N'                                                                                AS reversed
       ,m_rate_curr_lc                                                                     AS exchange_rate
       ,CASE WHEN account_doc_type='DG' THEN 'CM'
             WHEN account_doc_type ='AB' AND deb_cre_amt_dc<0 THEN 'CM'
             WHEN account_doc_type='AB' AND deb_cre_amt_dc>=0 THEN 'DM'
             WHEN account_doc_type='RV' AND deb_cre_amt_dc<0 THEN 'CM'
             WHEN account_doc_type='RV' AND deb_cre_amt_dc>=0 THEN 'INV'
             WHEN account_doc_type='DZ' AND deb_cre_amt_dc<0 THEN 'CM'
             WHEN account_doc_type='DZ' AND deb_cre_amt_dc>=0 THEN 'INV'
             WHEN account_doc_type='DR' THEN 'INV'
             WHEN account_doc_type='DA' THEN 'INV'  ELSE '' END                            AS "type"
       ,doc_curr                                                                           AS invoice_curr_code
       ,SUM(deb_cre_amt_lc)                                                                AS inv_amount
       ,SUM(deb_cre_amt_dc)                                                                AS amount
       ,SUM(deb_cre_amt_dc)                                                                AS revenue_amount
       ,null                                                                               AS tax_amount
       ,null                                                                               AS freight_amount
       ,SUM(unpaid_amt)                                                                    AS over_due_amount
       ,SUM(unpaid_amt_lc)                                                                 AS func_over_due_amount
       ,CASE WHEN net_due_date>CURRENT_DATE THEN SUM(unpaid_amt)  ELSE 0 END               AS curr_due_amount
       ,CASE WHEN net_due_date<CURRENT_DATE THEN SUM(unpaid_amt_lc)  ELSE 0 END            AS func_curr_due_amount
       ,SUM(deb_cre_amt_gc)                                                                AS cons_amount
       ,SUM(unpaid_amt_gc)                                                                 AS cons_over_due_amount
       ,CASE WHEN net_due_date>CURRENT_DATE THEN SUM(unpaid_amt_gc)  ELSE 0 END            AS cons_curr_due_amount
       ,m_rate_curr_gc                                                                     AS cons_exchange_rate
FROM glb_emagia.glb_fiar_ln_emagia ft
LEFT JOIN
(
	SELECT  distinct source_id
	       ,customer_number
	       ,customer_region
	       ,customer_reporting_country
	FROM glb_cust_mstr.glb_cust
	WHERE source_id ='APOLLO' 
)cust_region
ON ft.bill_to =cust_region.customer_number AND ft.source_id =cust_region.source_id
LEFT JOIN
(
	SELECT  billing_doc
	       ,source_id
	       ,sold_to
	       ,comp_code
	FROM glb_revenue.glb_revenue_ln
	WHERE source_id='APOLLO'
	GROUP BY  1
	         ,2
	         ,3
	         ,4
) rev
ON ft.account_doc =rev.billing_doc AND ft.source_id =rev.source_id AND ft.comp_code=rev.comp_code
LEFT JOIN
(
	SELECT  *
	FROM glb_cust_mstr.glb_cust
	WHERE (source_id IN ('APOLLO'))
)cust --subsq1
ON ft.source_id = cust.source_id AND ft.bill_to = cust.customer_number
LEFT JOIN glb_cust_mstr.glb_cust_gd
ON cust.grid_cust_num = glb_cust_mstr.glb_cust_gd.grid_cust_num
WHERE ft.source_id = 'APOLLO'
AND daily_monthly_flag = 'D'
AND account_doc_type IN ('DG', 'AB', 'RV', 'DZ', 'DR', 'DA')
AND ft.comp_code IN ('0120', '0150')
AND gl_account IN ('0000012100', '0000012920')
GROUP BY  ft.source_id
         ,"number"
         ,company_code
         ,sold_to_customer_id
         ,bill_to_customer_id
         ,trx_date
         ,trx_due_date
	 ,account_doc
         ,term
         ,region
         ,Intercompany_flag
         ,complete_flag
         ,gl_date
         ,reversed
         ,exchange_rate
         ,"type"
         ,invoice_curr_code
         ,cons_exchange_rate