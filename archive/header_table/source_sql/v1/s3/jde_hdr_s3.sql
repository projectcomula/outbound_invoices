select 
ft.source_id,
ft.billing_doc as unique_id
,ft.billing_doc as number 
,ft.comp_code as company_code
,rev.sold_to as sold_to_customer_id
,ft.bill_to as bill_to_customer_id
,ft.bill_date as trx_date
,net_due_date as trx_due_date
,invc_pmt_terms as term 
,cust_region.customer_region as region
,case 
when ft.source_id='JDE' and cust_gd.glb_industry_code_1 not in ('DOM','ADOM','PDOM','EC','FGN') then 'YES'
else 'NO'
end as Intercompany_flag
,case 
when unpaid_amt_gc =0 then 'Y'
when unpaid_amt_gc =deb_cre_amt_gc then 'N'
when unpaid_amt_gc<deb_cre_amt_gc and unpaid_amt_gc>0 then 'P'
else null
end as complete_flag
,posting_date as gl_date
,'N' as reserved
,m_rate_curr_lc as exchange_rate
,case 
when ft.bill_type='CO' then 'CM'
when ft.bill_type='RI' then 'INV'
when ft.bill_type='RJ' then 'DM'
when ft.bill_type='RM' then 'CM'
when ft.bill_type='RW' and deb_cre_amt_dc>0 then 'DM'
when ft.bill_type='RW' and deb_cre_amt_dc<0 then 'CM'
else ''
end as "type"
,doc_curr as invoice_curr_code
,sum(deb_cre_amt_lc) as inv_amout
,sum(deb_cre_amt_dc) as amount 
,sum(deb_cre_amt_dc) as revenue_amount
,sum(tax_amount / 100) as tax_amount 
,sum(fr.fr_net_revenue_dc) 
 as freight_amount 
,sum(unpaid_amt) as over_due_amount
,sum(unpaid_amt_lc) as func_over_due_amount
,case 
when net_due_date>CURRENT_DATE then  sum(unpaid_amt)
else 0
end as  cur_due_amount
,case 
when net_due_date<CURRENT_DATE then  sum(unpaid_amt_lc)
else 0
end as func_curr_due_amount
,sum(deb_cre_amt_gc) as cons_amount
,sum(unpaid_amt_gc) as cons_over_due_amount
,case 
when net_due_date>CURRENT_DATE then  sum(unpaid_amt_gc)
else 0
end as cons_curr_due_amount
,m_rate_curr_gc as cons_exchange_rate
from(
select  source_id ,billing_doc,comp_code,doc_curr,net_due_date,posting_date,bill_to, sum(deb_cre_amt_dc)as deb_cre_amt_dc,sum(deb_cre_amt_lc) as deb_cre_amt_lc,bill_type,invc_pmt_terms,
bill_date,sum(unpaid_amt) as unpaid_amt,invc_status_cd,sum(deb_cre_amt_gc)as deb_cre_amt_gc,m_rate_curr_lc,m_rate_curr_gc,sum(unpaid_amt_lc) as unpaid_amt_lc,
sum(unpaid_amt_gc) as unpaid_amt_gc ,sum(tax_amount) as tax_amount,daily_monthly_flag
from public.glb_fiar_ln_emagia where source_id ='JDE'and daily_monthly_flag ='D' and bill_type in ('CO', 'RI', 'RM', 'RW', 'RJ') group by  source_id ,billing_doc,comp_code,doc_curr,net_due_date,
posting_date,bill_to,bill_type,invc_pmt_terms,bill_date,invc_status_cd,m_rate_curr_lc,m_rate_curr_gc,daily_monthly_flag
)ft
--where ft.source_id ='JDE' and daily_monthly_flag='D'
left join 
(
select distinct
source_id ,
customer_number,
customer_region, 
customer_reporting_country 
from glb_cust_mstr.glb_cust where source_id ='JDE'
)cust_region
on ft.bill_to =cust_region.customer_number 
and ft.source_id =cust_region.source_id 
left join
(
select 
billing_doc,
source_id,
bill_to,
sold_to,
comp_code
from   glb_revenue.glb_revenue_ln where source_id='JDE'
group by 1,2,3,4,5
) rev 
on
ft.billing_doc =rev.billing_doc 
and ft.source_id =rev.source_id 
--and ft.bill_date=rev.bill_date
and ft.bill_to=rev.bill_to
and  ft.comp_code=rev.comp_code
left join 
(
select 
billing_doc
,source_id
,comp_code 
,typ.bill_type
,sum(fr_net_revenue_dc) as fr_net_revenue_dc
from 
(select 
billing_doc
,source_id
,comp_code 
,bill_type 
,material 
,sum(net_revenue_dc) as fr_net_revenue_dc
from   glb_revenue.glb_revenue_ln where source_id='JDE' and item_type ='F' 
--and material in ('36-FREIGHT','DE-FREIGHT','GB-FREIGHT','UK-FREIGHT','491610','491620','491621','491622','491916')
group by billing_doc
,source_id
,comp_code 
,material
,bill_type )typ
 join 
(
select imlitm from  jde_tables.jde_f4101_material where IMSTKT='V' and dl_active_flag=TRUE
)sto
on 
typ.material=sto.imlitm
group by 
billing_doc
,source_id
,comp_code 
,bill_type
)fr
on
ft.billing_doc = fr.billing_doc
and ft.source_id = fr.source_id
and ft.comp_code = fr.comp_code
and ft.bill_type = fr.bill_type
left join (
select
	*
from
	GLB_CUST_MSTR.glb_cust
where 
	 (source_id = 'JDE'	and account_group = 'B')
	)cust 
   on
ft.source_id = cust.source_id
and ft.bill_to = cust.customer_number
left join glb_cust_mstr.glb_cust_gd on
cust.grid_cust_num = cust_gd.grid_cust_num
/*
left join 
(
select distinct transaction_originator,sales_order,company_code,po_number from  glb_ordmgmt.glb_ord_mgmt_ln where source_id ='JDE' 
)od
on ft.so_number=od.sales_order
and ft.po_number=od.po_number
*/
where
ft.source_id = 'JDE'
and daily_monthly_flag = 'D'
and ft.bill_type in ('CO', 'RI', 'RM', 'RW', 'RJ')
group by
ft.source_id
,ft.billing_doc
,"number"
,company_code
,sold_to_customer_id
,bill_to_customer_id
,trx_date
,trx_due_date
,term
,region
,Intercompany_flag
,complete_flag
,gl_date
,reserved
,exchange_rate
,"type"
,invoice_curr_code
,cons_exchange_rate