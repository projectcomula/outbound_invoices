##############################################
######### IMPORTS & DECLARATION ##############
##############################################

import json
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame
from pyspark.sql.types import NullType
from pyspark.sql.types import StringType, TimestampType, IntegerType
import pyspark.sql.functions as F
from pyspark.sql.functions import lit
from pyspark.sql.functions import when, col
import boto3
from pyspark.sql.functions import udf
from datetime import datetime, date
today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print("today")
print(today)
# @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()


##############################################
############### EXTRACTION ###################
##############################################
redshift_details = {
    "url": "jdbc:redshift://vy-edw-insight-cluster-dev1.cm5u33ejahqp.us-east-2.redshift.amazonaws.com:5439/dve",
    "user": "etl_di_user01",
    "password": "etlVyD4v",
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

rs_tables = [
    "glb_emagia.glb_fiar_ln_emagia",
    "glb_revenue.glb_revenue_ln",
    "glb_cust_mstr.glb_cust_gd",
    "glb_cust_mstr.glb_cust",
    "sap_tables.sap_apollo_revenue_data"
    ]

try:

    rs_dyn_frames = []

    for table in rs_tables:
        redshift_details["dbtable"] = table
        dynamic_frame = glueContext.create_dynamic_frame_from_options(
            connection_type="redshift",
            connection_options=redshift_details,
            transformation_ctx="dynamic_frame"
            )
        rs_dyn_frames.append(dynamic_frame)
    logger.info("Dynamic frames created for all source tables")

except Exception as e:
    logger.exception(f"Exception during Dynamic frame formation :: {e.args}")

##############################################
############## TRANSFORMATION ################
##############################################

try:

    i = 0
    pyspark_tables = []

    for item in rs_dyn_frames:
        i += 1
        temp_tbl = f"temp_tbl_{i}"
        dataframe = item.toDF().repartition(1)
        dataframe.createOrReplaceTempView(temp_tbl)
        pyspark_tables.append(temp_tbl)

    logger.info("PySpark temp tables created for all source tables")

except Exception as e:
    logger.exception(f"Exception during temp table formation :: {e.args}")

try:
    
    s3 = boto3.client('s3')
    

    path = "aws-glue/outbound/source-sql/hdr_apollo"
    bucket = 'glb-emagia-dev'
    result = s3.list_objects(Bucket=bucket, Prefix=path)

    for item in result.get('Contents'):

        data = s3.get_object(Bucket=bucket, Key=item.get('Key'))
        contents = data['Body'].read()
        apollo_hquery = contents.decode("utf-8")
        
       
    logger.info(f"Before replace : {apollo_hquery}")

    for a, b in zip(rs_tables, pyspark_tables):
        apollo_hquery = apollo_hquery.replace(a, b)

    apollo_hquery = apollo_hquery.replace('"type"', 'atype')
    logger.info(f"After replace : {apollo_hquery}")

except Exception as e:

    logger.exception(f"Exception during s3 retreival {e.args}")

try:

    logger.info("Querying the temp tables for header table data")
    header_df = spark.sql(apollo_hquery)
    logger.info(f"Header DF count : {header_df.count()}")
    

except Exception as e:

    logger.exception(f"Exception during SQL {e.args}")

##############################################
################## LOAD ######################
##############################################
df2 = header_df.select([
    F.lit(None).cast('string').alias(i.name)
    if isinstance(i.dataType, NullType)
    else i.name
    for i in header_df.schema
])


# Write to Redshift
df2 = df2.withColumn("inv_amount",df2["inv_amount"].cast(StringType()))
df2 = df2.withColumn("amount",df2["amount"].cast(StringType()))
df2 = df2.withColumn("func_over_due_amount",df2["func_over_due_amount"].cast(StringType()))
df2 = df2.withColumn("revenue_amount",df2["revenue_amount"].cast(StringType()))
df2 = df2.withColumn("over_due_amount",df2["over_due_amount"].cast(StringType()))
df2 = df2.withColumn("curr_due_amount",df2["curr_due_amount"].cast(StringType()))
df2 = df2.withColumn("func_curr_due_amount",df2["func_curr_due_amount"].cast(StringType()))
df2 = df2.withColumn("cons_amount",df2["cons_amount"].cast(StringType()))
df2 = df2.withColumn("cons_over_due_amount",df2["cons_over_due_amount"].cast(StringType()))
df2 = df2.withColumn("cons_curr_due_amount",df2["cons_curr_due_amount"].cast(StringType()))
df2 = df2.withColumn("cons_exchange_rate",df2["cons_exchange_rate"].cast(StringType()))
df2 = df2.withColumn("exchange_rate",df2["exchange_rate"].cast(StringType()))
df2 = df2.withColumn('datekey',lit(today))
df2 = df2.withColumn("datekey",df2["datekey"].cast(TimestampType()))
df2 = df2.withColumnRenamed("atype","type")


#print(df2.printSchema())
header_dynamic_frame = DynamicFrame.fromDF(df2, glueContext, "header_dynamic_frame")

try:
    redshift_details["dbtable"] = "glb_emagia.glb_ar_col_emagia_hdr"
    yesterdays_data_frame = glueContext.create_dynamic_frame_from_options(
            connection_type="redshift",
            connection_options=redshift_details,
            transformation_ctx="dynamic_frame"
            )
    yesterdays_data_frame = yesterdays_data_frame.toDF().repartition(1)
    yesterdays_data_frame.createOrReplaceTempView("yesterday_temp_tbl")
    yesterdays_data_frame = spark.sql("select source_id,unique_id,number,company_code,sold_to_customer_id,bill_to_customer_id,  trx_date,trx_due_date,term,region,Intercompany_flag,complete_flag,gl_date,reversed,exchange_rate,type,invoice_curr_code,inv_amount,amount,revenue_amount,tax_amount,freight_amount,over_due_amount,func_over_due_amount,curr_due_amount,func_curr_due_amount,cons_amount,cons_over_due_amount,cons_curr_due_amount,cons_exchange_rate from yesterday_temp_tbl where source_id='APOLLO' and datekey='2021-08-31 17:02:34'")   
    print("yesterdays_data_frame fetched")
    
      
except Exception as e:
    logger.exception(f"Exception during s3 write {e.args}")
    
# Compare logic
print(yesterdays_data_frame.count())
print(df2.count())
#Qualified_df=yesterdays_data_frame.subtract(df2)
#df2 = df2.withColumn("type", when(yesterdays_data_frame["unique_id"] == "9100525274","CM"))
#df2 = df2.withColumn("type", when(col("unique_id") == "1400128490","INV"))
#print("Modified two rows")

df2=yesterdays_data_frame.alias('df2')
yesterdays_data_frame = yesterdays_data_frame.withColumn("Source1", lit("DF1"))
df2 = df2.withColumn("Source2", lit("DF2"))
#temporary columns to refer the origin later
yesterdays_data_frame=yesterdays_data_frame.drop("datekey")
df2=df2.drop("datekey")
print("yesterdays_data_frame")
#print(yesterdays_data_frame.printSchema())
#print("df2")
#print(df2.printSchema())


yesterdays_data_frame = yesterdays_data_frame.withColumn("curr_due_amount", yesterdays_data_frame["curr_due_amount"].cast(IntegerType()))
yesterdays_data_frame = yesterdays_data_frame.withColumn("over_due_amount", yesterdays_data_frame["over_due_amount"].cast(IntegerType()))
yesterdays_data_frame = yesterdays_data_frame.withColumn("func_over_due_amount", yesterdays_data_frame["func_over_due_amount"].cast(IntegerType()))
yesterdays_data_frame = yesterdays_data_frame.withColumn("func_curr_due_amount", yesterdays_data_frame["func_curr_due_amount"].cast(IntegerType()))
yesterdays_data_frame = yesterdays_data_frame.withColumn("cons_curr_due_amount", yesterdays_data_frame["cons_curr_due_amount"].cast(IntegerType())) 
yesterdays_data_frame = yesterdays_data_frame.withColumn("cons_over_due_amount", yesterdays_data_frame["cons_over_due_amount"].cast(IntegerType())) 


df2 = df2.withColumn("curr_due_amount", df2["curr_due_amount"].cast(IntegerType()))
df2 = df2.withColumn("over_due_amount", df2["over_due_amount"].cast(IntegerType()))
df2 = df2.withColumn("func_over_due_amount", df2["func_over_due_amount"].cast(IntegerType()))
df2 = df2.withColumn("func_curr_due_amount", df2["func_curr_due_amount"].cast(IntegerType()))
df2 = df2.withColumn("cons_curr_due_amount", df2["cons_curr_due_amount"].cast(IntegerType())) 
df2 = df2.withColumn("cons_over_due_amount", df2["cons_over_due_amount"].cast(IntegerType())) 

print("yesterdays_data_frame")
print(yesterdays_data_frame.show())
print("DF2")
print(df2.show())
yesterdays_data_frame=yesterdays_data_frame.na.fill(value=0)
df2=df2.na.fill(value=0)
yesterdays_data_frame.na.fill(value=0,subset=["sold_to_customer_id"])
yesterdays_data_frame.na.fill(value=0,subset=["tax_amount"])
yesterdays_data_frame.na.fill(value=0,subset=["freight_amount"])
df2.na.fill(value=0,subset=["sold_to_customer_id"])
df2.na.fill(value=0,subset=["tax_amount"])
df2.na.fill(value=0,subset=["freight_amount"])

#print(Qualified_df.printSchema())    

Qualified_df=yesterdays_data_frame.join(df2, ["source_id","unique_id","number","company_code","sold_to_customer_id","bill_to_customer_id","trx_date","trx_due_date","term","region","Intercompany_flag","complete_flag","gl_date","reversed","exchange_rate","type","invoice_curr_code","inv_amount","amount","revenue_amount","tax_amount","freight_amount","over_due_amount","func_over_due_amount","curr_due_amount","func_curr_due_amount","cons_amount","cons_over_due_amount","cons_curr_due_amount","cons_exchange_rate"],"full").withColumn("FLAG",when(col("Source1").isNotNull() & col("Source2").isNotNull(), "SAME").otherwise(when(col("Source1").isNotNull(), "DF1").otherwise("DF2")))


print(Qualified_df.count())
leftdf=Qualified_df.filter(Qualified_df.FLAG == "DF1").collect()
bothdf=Qualified_df.filter(Qualified_df.FLAG == "DF2").collect()
rightdf=Qualified_df.filter(Qualified_df.FLAG == "SAME").collect()



#print("LDF",leftdf.count())
#rint("RDF",rightdf.count())
#print("BDF",bothdf.count())

#yesterdays_data_frame = yesterdays_data_frame.drop("datekey")
#df2 = df2.drop("datekey")
#Qualified_df=df2.subtract(yesterdays_data_frame)
#print(Qualified_df.count())
#df = dataframe_difference(yesterdays_data_frame,df2,which='both')
#df1=dataframe_difference(yesterdays_data_frame,df2,which=None)
#print("Dataframes Compared")
#df=df.append(df1)

#Qualified_df = df[df['_merge'] == "right_only"]
#print("Qualified_df")
#print(Qualified_df)
'''
# Write to csv
'''
def scientific_to_numeric(decimal_string):
    x = decimal_string.split(".")
    if len(x) > 1:
        b = all(item in '0' for item in x[1])
        if b:
            numeric_val = int(v)
        else:
            numeric_val = decimal_string
    else:
        numeric_val = int
    return numeric_val 

#example_udf = udf(scientific_to_numeric, StringType())
#Qualified_df=Qualified_df.withColumn('curr_due_amount_new', example_udf(Qualified_df.curr_due_amount))
            

print(Qualified_df.show())

      
try:
    S3_PATH = f"s3://glb-emagia-dev/aws-glue/outbound/output-files/Apollo/header-table/{date.today().strftime('%d%m%Y')}"
    Qualified_df.coalesce(1).write.option("header","true").csv(S3_PATH)
    #Qualified_df.coalesce(1).write.option("header","true").csv(S3_PATH)
    

except Exception as e:

    logger.exception(f"Exception during s3 write {e.args}")



'''
try:

    redshift_details["dbtable"] = "glb_emagia.glb_ar_col_emagia_hdr"
    glueContext.write_dynamic_frame.from_options(frame=header_dynamic_frame, connection_type="redshift", connection_options=redshift_details)
    logger.info("Data written to redshift")

except Exception as e:
    logger.exception(f"Exception during redshift write {e.args}")
'''
logger.info("job completed")
job.commit()
