##############################################
######### IMPORTS & DECLARATION ##############
##############################################

import sys
import boto3

from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

import pyspark.sql.functions as func
from pyspark.context import SparkContext
from pyspark.sql.types import NullType, StringType, TimestampType, IntegerType
from pyspark.sql.functions import lit, when, col, udf

from datetime import datetime, date

# @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()

today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
logger.info(f"Today's Date {today}")

##############################################
############### EXTRACTION ###################
##############################################

# Forming Redshift connection option
redshift_details = {
    "url": "jdbc:redshift://vy-edw-insight-cluster-dev1.cm5u33ejahqp.us-east-2.redshift.amazonaws.com:5439/dve",
    "user": "etl_di_user01",
    "password": "etlVyD4v",
    "redshiftTmpDir": "s3://consolidatedar/tempdir"
}

rs_tables = [
    "glb_emagia.glb_fiar_ln_emagia",
    "glb_revenue.glb_revenue_ln",
    "glb_cust_mstr.glb_cust",
    "sap_tables.sap_olympus_revenue_data"
    ]

# Form Spark Dynamic frames for above list of tables
try:
    rs_dyn_frames = []

    for table in rs_tables:
        redshift_details["dbtable"] = table
        dynamic_frame = glueContext.create_dynamic_frame_from_options(
            connection_type="redshift",
            connection_options=redshift_details,
            transformation_ctx="dynamic_frame"
            )
        rs_dyn_frames.append(dynamic_frame)

    logger.info("Dynamic frames created for all source tables")

except Exception as e:
    logger.exception(f"Exception during Dynamic frame formation :: {e.args}")

##############################################
############## TRANSFORMATION ################
##############################################

# Form PySpark temp tables for above list of Dynamic frames
try:
    i = 0
    pyspark_tables = []

    for item in rs_dyn_frames:
        i += 1
        temp_tbl = f"temp_tbl_{i}"
        dataframe = item.toDF().repartition(1)
        dataframe.createOrReplaceTempView(temp_tbl)
        pyspark_tables.append(temp_tbl)

    logger.info("PySpark temp tables created for all source tables")

except Exception as e:
    logger.exception(f"Exception during temp table formation :: {e.args}")


# Extract SQL query from s3 bucket
try:
    s3 = boto3.client('s3')
    path = "aws-glue/outbound/source-sql/hdr_Olympus"
    bucket = 'glb-emagia-dev'
    result = s3.list_objects(Bucket=bucket, Prefix=path)

    for item in result.get('Contents'):

        data = s3.get_object(Bucket=bucket, Key=item.get('Key'))
        contents = data['Body'].read()
        olympus_header_query = contents.decode("utf-8")
       
    logger.info(f"Query Before replace :: {olympus_header_query}")

    # Replace actual table names with temp table names in source sql
    for a, b in zip(rs_tables, pyspark_tables):
        olympus_header_query = olympus_header_query.replace(a, b)

    olympus_header_query = olympus_header_query.replace('"type"', 'atype')
    logger.info(f"Query After replace :: {olympus_header_query}")

except Exception as e:
    logger.exception(f"Exception during s3 retreival {e.args}")


# Run the transformed SQL on PySPark Temp tables
try:
    logger.info("Querying the temp tables for header table data")
    header_df = spark.sql(olympus_header_query)
    logger.info(f"Query executed successfully - No of rows retreived :: {header_df.count()}")
    print(f"Header DF (today) schema before conversions :: {header_df.printSchema()}")

except Exception as e:
    logger.exception(f"Exception during SQL execution :: {e.args}")

    """Explanation of below line
    
    For every column of in the header_df --> StructType(List(StructField(col_name, data_type, nullable: bool)))
    if column is of type NullType, cast it to string, create a new col using lit, name it as the original col so it gets replaced with new datatype
    else, main the same col name and type

    Returns:
        [PySpark dataframe]: [Above if else is because below statement returns a new dataframe and all cols from source df should be maintained]
    """
 
# Change NullType cols to string
today_dataframe = header_df.select([func.lit(None).cast('string').alias(i.name) if isinstance(i.dataType, NullType) else i.name for i in header_df.schema])

# Change Decimal cols to string
amt_cols = [
    "inv_amount",
    "amount",
    "func_over_due_amount",
    "revenue_amount",
    "over_due_amount",
    "curr_due_amount",
    "func_curr_due_amount",
    "cons_amount",
    "cons_over_due_amount",
    "cons_curr_due_amount",
    "cons_exchange_rate",
    "exchange_rate",
    "tax_amount"
]

for col in amt_cols:
    today_dataframe = today_dataframe.withColumn(col, today_dataframe[col].cast(StringType()))

today_dataframe = today_dataframe.withColumnRenamed("atype","type")

print(f"Header DF (today) schema after conversions :: {today_dataframe.printSchema()}")

def scientific_to_numeric(decimal_string):
    x = decimal_string.split(".")
    if len(x) > 1:
        b = all(item in '0' for item in x[1])
        if b:
            numeric_val = int(x[0])
        else:
            numeric_val = decimal_string
    else:
        y = decimal_string.split("-")
        if 'E' in y[0]:
            numeric_val = 0
        else:
            numeric_val = int(x[0])
    return numeric_val 

conv_udf = udf(scientific_to_numeric, StringType())

dbcols = today_dataframe.schema()
logger.info(f"Database cols : {dbcols}")

today_dataframe = today_dataframe.withColumn("inv_amount", conv_udf(today_dataframe.inv_amount))
today_dataframe = today_dataframe.withColumn("amount", conv_udf(today_dataframe.amount))
today_dataframe = today_dataframe.withColumn("func_over_due_amount", conv_udf(today_dataframe.func_over_due_amount))
today_dataframe = today_dataframe.withColumn("revenue_amount", conv_udf(today_dataframe.revenue_amount))
today_dataframe = today_dataframe.withColumn("over_due_amount", conv_udf(today_dataframe.over_due_amount))
today_dataframe = today_dataframe.withColumn("curr_due_amount", conv_udf(today_dataframe.curr_due_amount))
today_dataframe = today_dataframe.withColumn("func_curr_due_amount", conv_udf(today_dataframe.func_curr_due_amount))
today_dataframe = today_dataframe.withColumn("cons_amount", conv_udf(today_dataframe.cons_amount))
today_dataframe = today_dataframe.withColumn("cons_over_due_amount", conv_udf(today_dataframe.cons_over_due_amount))
today_dataframe = today_dataframe.withColumn("cons_curr_due_amount", conv_udf(today_dataframe.cons_curr_due_amount))
today_dataframe = today_dataframe.withColumn("cons_exchange_rate", conv_udf(today_dataframe.cons_exchange_rate))
today_dataframe = today_dataframe.withColumn("exchange_rate", conv_udf(today_dataframe.exchange_rate))

logger.info(f"Today's DF after UDF :: {today_dataframe.show()}")

# Read contents from header table to form yest_dataframe

try:
    redshift_details["dbtable"] = "glb_emagia.glb_ar_col_emagia_hdr"

    yest_dataframe = glueContext.create_dynamic_frame_from_options(
            connection_type="redshift",
            connection_options=redshift_details,
            transformation_ctx="dynamic_frame"
            )

    yest_dataframe = yest_dataframe.toDF().repartition(1) # Convert to PySpark dataframe
        
    yest_dataframe.createOrReplaceTempView("yesterday_temp_tbl") # Create temp table to run SQL
    yest_dataframe = spark.sql("select * from yesterday_temp_tbl where source_id='OLYMPUS' and datekey='2021-09-05 10:35:04'")   
    logger.info("Formed yest_dataframe successfully")
    
    
except Exception as e:
    logger.exception(f"Exception during forming yest_dataframe :: {e.args}")

# Compare logic
yest_dataframe = yest_dataframe.drop("datekey")
logger.info(f"No of rows in yest_dataframe :: {yest_dataframe.count()} and in today_dataframe :: {today_dataframe.count()}")

qualified_df = today_dataframe.exceptAll(yest_dataframe)
logger.info(f"Qualified dataframe :: {qualified_df.show()}")


# Create new col with today's date
today_dataframe = today_dataframe.withColumn('datekey', lit(today))
today_dataframe = today_dataframe.withColumn("datekey", today_dataframe["datekey"].cast(TimestampType()))



##############################################
################## LOAD ######################
##############################################

# Write to s3   

##############################################
################## LOAD ######################
##############################################
'''
# Write to s3   
'''
try:
    today_dataframe=today_dataframe.coalesce(1)
    S3_PATH = f"s3://glb-emagia-dev/aws-glue/outbound/output-files/Olympus/header-table/{date.today().strftime('%d%m%Y')}"
    qualified_df.coalesce(1).write.option("header","true").csv(S3_PATH)

except Exception as e:
    logger.exception(f"Exception during s3 write :: {e.args}")

# Write to redshift


header_dynamic_frame = DynamicFrame.fromDF(today_dataframe, glueContext, "header_dynamic_frame")
try:
    redshift_details["dbtable"] = "glb_emagia.glb_ar_col_emagia_hdr"
    glueContext.write_dynamic_frame.from_options(frame=header_dynamic_frame, connection_type="redshift", connection_options=redshift_details)
    logger.info("Data written to redshift")

except Exception as e:
    logger.exception(f"Exception during redshift write :: {e.args}")

logger.info("job completed")
job.commit()
