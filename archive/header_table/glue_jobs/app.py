##############################################
######### IMPORTS & DECLARATION ##############
##############################################

import sys
import boto3

from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from awsglue.context import GlueContext
from awsglue.job import Job

from pyspark.context import SparkContext
from pyspark.sql.types import TimestampType
from pyspark.sql.functions import lit, when, col, udf

from datetime import datetime
from pyutils import PySparkUtils

# @params: [JOB_NAME]

args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
logger = glueContext.get_logger()

# @variables

today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
logger.info(f"Today's Date {today}")
rs_secret = "redshift_dev_cluster_etldiuser"
s3_temp_dir = "s3://consolidatedar/tempdir"
target_dir = "s3://glb-emagia-dev/aws-glue/outbound/output-files/Apollo/header-table"
target_table = "glb_emagia.glb_ar_col_emagia_hdr"

##############################################
############### EXTRACTION ###################
##############################################


# Forming Redshift connection option
redshift_details = PySparkUtils().get_redshift_conn_details(rs_secret, s3_temp_dir)

# Form list of tables present in SQL
rs_tables = [
    "glb_emagia.glb_fiar_ln_emagia",
    "glb_revenue.glb_revenue_ln",
    "glb_cust_mstr.glb_cust",
    "sap_tables.sap_olympus_revenue_data"
    ]

# Form Spark Dynamic frames for above list of tables
rs_dyn_frames = PySparkUtils().get_spark_dynamic_frame(rs_tables, glueContext, redshift_details)

##############################################
############## TRANSFORMATION ################
##############################################

# Form PySpark temp tables for above list of Dynamic frames
pyspark_tables = PySparkUtils().form_pyspark_temp_tables(rs_dyn_frames)


# Extract SQL query from s3 bucket
try:
    s3 = boto3.client('s3')
    path = "aws-glue/outbound/source-sql/hdr_Olympus"
    bucket = 'glb-emagia-dev'
    result = s3.list_objects(Bucket=bucket, Prefix=path)

    for item in result.get('Contents'):

        data = s3.get_object(Bucket=bucket, Key=item.get('Key'))
        contents = data['Body'].read()
        olympus_header_query = contents.decode("utf-8")
       
    logger.info(f"Query Before replace :: {olympus_header_query}")

    # Replace actual table names with temp table names in source sql
    for a, b in zip(rs_tables, pyspark_tables):
        olympus_header_query = olympus_header_query.replace(a, b)

    olympus_header_query = olympus_header_query.replace('"type"', 'atype')
    logger.info(f"Query After replace :: {olympus_header_query}")

except Exception as e:
    logger.exception(f"Exception during s3 retreival {e.args}")


# Run the transformed SQL on PySpark Temp tables
try:
    logger.info("Querying the temp tables for header table data")
    header_df = spark.sql(olympus_header_query)
    logger.info(f"Query executed successfully - No of rows retreived :: {header_df.count()}")
    print(f"Header DF (today) schema before conversions :: {header_df.printSchema()}")

except Exception as e:
    logger.exception(f"Exception during SQL execution :: {e.args}")


# Change Decimal cols to string
amt_cols = [
    "inv_amount",
    "amount",
    "func_over_due_amount",
    "revenue_amount",
    "over_due_amount",
    "curr_due_amount",
    "func_curr_due_amount",
    "cons_amount",
    "cons_over_due_amount",
    "cons_curr_due_amount",
    "cons_exchange_rate",
    "exchange_rate"
]

today_dataframe = PySparkUtils().change_col_datatype(header_df, amt_cols)
today_dataframe = PySparkUtils().scientific_to_numeric(today_dataframe)

yest_dynframe = PySparkUtils().get_spark_dynamic_frame(["glb_emagia.glb_ar_col_emagia_hdr"], glueContext, redshift_details)
yest_dataframe = yest_dynframe.toDF().repartition(1) # Convert to PySpark dataframe
    
yest_dataframe.createOrReplaceTempView("yesterday_temp_tbl") # Create temp table to run SQL
yest_dataframe = spark.sql("select * from yesterday_temp_tbl where source_id='APOLLO' and datekey='2021-09-01 19:03:15'")   
yest_dataframe = yest_dataframe.drop("datekey")

logger.info("Formed yest_dataframe successfully")

# Compare logic
logger.info(f"No of rows in yest_dataframe :: {yest_dataframe.count()} and in today_dataframe :: {today_dataframe.count()}")

qualified_df = today_dataframe.exceptAll(yest_dataframe)
logger.info(f"Qualified dataframe :: {qualified_df.show()}")

# Create new col with today's date
today_dataframe = today_dataframe.withColumn('datekey', lit(today))
today_dataframe = today_dataframe.withColumn("datekey", today_dataframe["datekey"].cast(TimestampType()))

##############################################
################## LOAD ######################
##############################################

# Write to s3   

PySparkUtils().write_to_s3(qualified_df, target_dir)

# Write to redshift

PySparkUtils().write_to_redshift(today_dataframe, glueContext, redshift_details, target_table)

logger.info("job completed")
job.commit()
